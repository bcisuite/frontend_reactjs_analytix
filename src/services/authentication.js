import { fetchPost } from "../utils/fetch";

export const goLogout = async (data) => {
    return await fetchPost("/v2/account/logout", data);
};

export const goLoginSso = async (data) => {
    return await fetchPost("/v2/account/login/sso", data);
};

export const doRefreshToken = async (data) => {
    return await fetchPost("/v2/account/token/refresh", data, false);
};
