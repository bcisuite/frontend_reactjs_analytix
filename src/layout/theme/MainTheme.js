import { createTheme } from '@mui/material/styles';
import styleVal from "../../styles/variable";

const MainTheme = createTheme({
    components: {
        MuiPopover: {
            styleOverrides: {
                root: {
                    background: "rgba(0,0,0,.2)",
                    "&#menu-": {
                        backgroundColor: "transparent",
                    },
                },
                paper: {
                    borderRadius: styleVal["border-radius-regular"],
                },
            },
        },
        MuiButton: {
            styleOverrides: {
                root: {
                    outline: "none !important",
                    minWidth: 32,
                    textTransform: "normal",
                    fontWeight: 700,
                    backgroundColor: "#FFFFFF",
                },
            },
        },
        MuiFormControl: {
            styleOverrides: {
                root: {
                    maxWidth: "100%",
                    "&.form-control-full": {
                        width: "100%",
                    },
                },
            },
        },
        MuiOutlinedInput: {
            styleOverrides: {
                root: {
                    backgroundColor: "#FFFFFF",
                    "&.Mui-error fieldset": {
                        borderColor: "red !important",
                    },
                },
                input: {
                    padding: 10,
                    fontSize: 12,
                },
                notchedOutline: {
                    borderColor: "#E9E9E9",
                },
            },
        },
        MuiCheckbox: {
            styleOverrides: {
                root: {
                    color: "#CAC8C8",
                    padding: 6.5,
                    "&.nopadding": {
                        padding: 0,
                    },
                },
                indeterminate: {
                    color: styleVal["red-bci"],
                },
            },
        },
        MuiRadio: {
            styleOverrides: {
                root: {
                    color: "#CAC8C8",
                },
            },
        },
        MuiPagination: {
            styleOverrides: {
                ul: {
                    "& li:last-child button.Mui-disabled, & li:first-child button.Mui-disabled":
                    {
                        backgroundColor: "#FFFFFF",
                        borderColor: "#E8E8E8",
                        opacity: 1,
                        "& path": {
                            color: "#6A6A73",
                        },
                    },
                    "& li:last-child button, & li:first-child button": {
                        backgroundColor: styleVal["red-bci"],
                        "& path": {
                            color: "#FFFFFF",
                        },
                    },
                },
            },
        },
        MuiPaginationItem: {
            styleOverrides: {
                page: {
                    background: "#FFFFFF",
                    "&$selected": {
                        backgroundColor: "#FFFFFF",
                        borderColor: styleVal["red-bci"],
                    },
                },
            },
        },
        MuiAccordionSummary: {
            styleOverrides: {
                root: {
                    minHeight: "0 !important",
                },
            },
        },
        MuiBackdrop: {
            styleOverrides: {
                root: {
                    // backgroundColor: 'rgba(0,0,0,.2)'
                },
            },
        },
        MuiDialog: {
            styleOverrides: {
                paper: {
                    borderRadius: 8,
                    boxShadow: "none",
                },
                paperWidthSm: {
                    maxWidth: 816,
                },
            },
        },
        MuiDialogTitle: {
            styleOverrides: {
                root: {
                    padding: "20px 22px",
                    fontSize: 16,
                    fontWeight: 700,
                    color: "#ff0000",
                    borderBottom: "1px solid #E8E8E8",
                },
            },
        },
        MuiDialogContent: {
            styleOverrides: {
                root: {
                    padding: 0,
                },
            },
        },
        MuiDialogActions: {
            styleOverrides: {
                root: {
                    padding: "21px 31px",
                    borderTop: "1px solid #E8E8E8",
                },
            },
        },
        MuiCard: {
            styleOverrides: {
                root: {
                    borderRadius: "8px",
                    boxShadow: "2px 2px 10px rgba(0,0,0,0.05)",
                    border: "1px solid #E8E8E8",
                },
            },
        },
        MuiCardContent: {
            styleOverrides: {
                root: {
                    padding: "12px",
                    "&:last-child": {
                        paddingBottom: "auto",
                    },
                },
            },
        },
        MuiTooltip: {
            styleOverrides: {
                tooltip: {
                    backgroundColor: styleVal.white,
                    border: `1px solid ${styleVal["grey-regular"]}`,
                    color: "black",
                },
                arrow: {
                    backgroundColor: styleVal.white,
                    color: styleVal["grey-regular"],
                },
            },
        },
        MuiAlert: {
            styleOverrides: {
                standardSuccess: {
                    backgroundColor: "#FFF1E6",
                },
                standardInfo: {
                    backgroundColor: "rgba(0, 57, 255, 0.05)",
                    border: "1px solid rgba(0, 57, 255, 0.1)",
                },
            },
        },
        MuiTableCell: {
            styleOverrides: {
                stickyHeader: {
                    backgroundColor: "#FFFFFF",
                },
            },
        },
        MuiPickersToolbarButton: {
            styleOverrides: {
                toolbarBtn: {
                    backgroundColor: "transparent",
                },
            },
        },
    },
    typography: {
        subtitle1: {
            fontSize: 18,
            lineHeight: "21.41px",
        },
        subtitle2: {
            fontSize: 16,
            lineHeight: "18.75px",
        },
        caption: {
            fontSize: 14,
            lineHeight: "21.41px",
        },
        body1: {
            fontSize: 12,
            lineHeight: "16.06px",
        },
    },
    palette: {
        primary: {
            main: "#FF0000", // red-bci
        },
        secondary: {
            main: "#808080", // grey-cool-bci
            dark: "#454550", // black-bci
            light: "#E8E8E8", // grey-regular-bci
            contrastText: "#F7F7F7", // grey-light-bci
        },
        info: {
            main: "#454550", // blue-bci
        },
        warning: {
            main: "#03989E", //tortoise-bci,
            light: "#D8F4F5", //tortoise-light-bci
        },
        success: {
            main: "#0DA738", //tortoise-bci,
            light: "#0DA738", //tortoise-light-bci
        },
        text: {
            primary: "#454550", // black-bci
            secondary: "#454550", // blue-bci
        },
        error: {
            main: "#FDA51A",
        },
    },
});

export default MainTheme;
