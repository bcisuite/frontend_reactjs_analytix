import { useEffect } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import { StyledEngineProvider, ThemeProvider } from '@mui/material/styles';
import { SnackbarProvider } from "notistack";

import MainTheme from "../theme/MainTheme";
import "./MainLayout.scss";

import Sidebar from "../../components/Sidebar/Sidebar";
import Topbar from "../../components/Topbar/Topbar";
import TNLMLoader from "../../components/TNLMLoader";
import TNLMWarningForceLoggedOutInactive from "../../components/TNLMWarning/ForceLoggedOutInactive";
import TNLMWarningForceLoggedOutTokenExpired from "../../components/TNLMWarning/ForceLoggedOutTokenExpired";
import TNLMZendesk from "../../components/TNLMZendesk";

import routes from "../../Routes";


import { setUserData } from "../../store/reducers/user/action";
import { closeGlobalLoader } from "../../store/reducers/layout/action";

import ssoConfig from "../../config/sso.config";

const MainLayout = ({ match }) => {
    const dispatch = useDispatch();

    const {
        globalLoaderState,
        forceLoggedOutInactive,
        forceLoggedOutTokenExpire,
    } = useSelector((state) => state.layout);

    // const { userData } = useSelector((state) => state.user);
    const userData = { id: 100, };

    useEffect(() => {
        let userDataLocalstorage = JSON.parse(localStorage.getItem("profile"));
        dispatch(setUserData({ payload: userDataLocalstorage }));
        // reset global loader when refresh
        dispatch(closeGlobalLoader());
    }, [dispatch]);

    const doLogoutSsoForCurrentSession = () => {
        localStorage.setItem("accessToken", "undefined-token");
        window.location.href = `${ssoConfig.tnlmBaseUri}/logout?app=${ssoConfig.tnlmAppName}`;
    };

    return Number(userData.id) > 0 ? (
        <StyledEngineProvider injectFirst>
            <ThemeProvider theme={MainTheme}>
                <CssBaseline />
                <SnackbarProvider maxSnack={5}>
                    {!(forceLoggedOutTokenExpire || forceLoggedOutInactive) && (
                        <div id="main-layout">
                            <Sidebar routes={routes} />
                            <div className="main-layout-content">
                                <Topbar />
                                <div className="main-content">
                                    <Container className="main-content-container" maxWidth={false}>
                                        <Switch>
                                            {routes.map((value, idx) => {
                                                return (
                                                    <Route
                                                        key={idx}
                                                        component={value.component}
                                                        path={`${value.layout}${value.path}`}
                                                    />
                                                );
                                            })}
                                            <Redirect
                                                from={match.path}
                                                to={`${match.path}/home`}
                                            ></Redirect>
                                        </Switch>
                                    </Container>
                                </div>
                            </div>
                        </div>
                    )}
                </SnackbarProvider>
                <TNLMLoader open={globalLoaderState} />
                <TNLMWarningForceLoggedOutInactive
                    onHandleSubmit={doLogoutSsoForCurrentSession}
                    show={forceLoggedOutInactive}
                />
                <TNLMWarningForceLoggedOutTokenExpired
                    onHandleSubmit={doLogoutSsoForCurrentSession}
                    show={forceLoggedOutTokenExpire}
                />
                <TNLMZendesk
                    defer
                    onLoaded={() => console.log("Help center is loaded")}
                />
            </ThemeProvider>
        </StyledEngineProvider>
    ) : null;
};

export default MainLayout;
