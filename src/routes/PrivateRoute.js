import React from 'react';
import { Route, useLocation } from 'react-router-dom';
import SSORedirectRoute from '../components/TNLMRedirect';

const user = localStorage.getItem('profile');
const subsId = localStorage.getItem('subscriptionId');

const PrivateRoute = ({ component: Component, ...rest }) => {
    const location = useLocation();

    let redirect = `${location.pathname}${location.search}`;
    if (redirect.indexOf('/logout') === 0) {
        redirect = '/main/home'
    }
    localStorage.setItem('redirect', redirect);

    return (
        <Route
            {...rest}
            render={props =>
                user && subsId ? (
                    <Component {...props} />
                ) : (
                    <SSORedirectRoute />
                )
            }
        />
    )
};

export default PrivateRoute;