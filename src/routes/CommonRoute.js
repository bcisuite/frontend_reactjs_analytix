import React from 'react';
import { Route, Redirect, useLocation } from 'react-router-dom';

const user = localStorage.getItem('profile');
const subsId = localStorage.getItem('subscriptionId');

const CommonRoute = ({ component: Component, ...rest }) => {
    const location = useLocation();

    return (
        <Route
            {...rest}
            render={props =>
                !user && !subsId ? (
                    <Component {...props} />
                ) : (
                    <Redirect to={new URLSearchParams(location.search)?.get('redirect') ? location.search.replace('?redirect=', '') : '/'} />
                )
            }
        />
    )
};

export default CommonRoute;