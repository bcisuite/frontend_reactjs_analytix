import { combineReducers } from "redux";
import Layout from "./reducers/layout";
import User from "./reducers/user";

export default combineReducers({
    layout: Layout,
    user: User,
});
