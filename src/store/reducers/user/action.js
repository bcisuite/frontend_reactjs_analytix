import {
    SET_USER_DATA,
} from "./types";

const setUserData = (response) => (dispatch) => {
    const { payload } = response;
    dispatch({
        type: SET_USER_DATA,
        payload,
    });
};

export { setUserData };
