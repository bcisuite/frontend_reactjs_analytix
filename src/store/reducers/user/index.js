import initialState from "./initialState";
import {
    SET_USER_DATA,
} from "./types";

const userReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_USER_DATA: {
            return {
                ...state,
                userData: payload,
            };
        }

        default:
            return state;
    }
};

export default userReducer;
