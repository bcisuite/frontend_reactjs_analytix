import initialState from "./initialState";
import {
    SET_FORCE_LOGGED_OUT_INACTIVE,
    SET_FORCE_LOGGED_OUT_TOKEN_EXPIRE,
    SET_GLOBAL_LOADER_STATE,
    SET_MORE_APP_MODAL,
} from "./types";

const layoutReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_GLOBAL_LOADER_STATE: {
            return {
                ...state,
                globalLoaderState: payload,
            };
        }
        case SET_MORE_APP_MODAL: {
            return {
                ...state,
                moreApp: !state.moreApp,
            };
        }
        case SET_FORCE_LOGGED_OUT_INACTIVE: {
            return {
                ...state,
                forceLoggedOutInactive: payload,
            };
        }
        case SET_FORCE_LOGGED_OUT_TOKEN_EXPIRE: {
            return {
                ...state,
                forceLoggedOutTokenExpire: payload,
            };
        }
        default:
            return state;
    }
};

export default layoutReducer;
