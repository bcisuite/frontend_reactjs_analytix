export const SET_GLOBAL_LOADER_STATE = "SET_GLOBAL_LOADER_STATE";
export const SET_MORE_APP_MODAL = "SET_MORE_APP_MODAL";
export const SET_FORCE_LOGGED_OUT_INACTIVE = "SET_FORCE_LOGGED_OUT_INACTIVE";
export const SET_FORCE_LOGGED_OUT_TOKEN_EXPIRE = "SET_FORCE_LOGGED_OUT_TOKEN_EXPIRE";
