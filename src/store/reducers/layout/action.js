import {
    SET_GLOBAL_LOADER_STATE,
    SET_MORE_APP_MODAL,
    SET_FORCE_LOGGED_OUT_INACTIVE,
    SET_FORCE_LOGGED_OUT_TOKEN_EXPIRE,
} from "./types";

const openGlobalLoader = () => (dispatch) => {
    dispatch({
        type: SET_GLOBAL_LOADER_STATE,
        payload: true,
    });
};

const closeGlobalLoader = () => (dispatch) => {
    dispatch({
        type: SET_GLOBAL_LOADER_STATE,
        payload: false,
    });
};

const openMoreApp = () => (dispatch) => {
    dispatch({
        type: SET_MORE_APP_MODAL,
    });
};

const setForceLoggedOutInactive = (payload) => (dispatch) => {
    dispatch({
        type: SET_FORCE_LOGGED_OUT_INACTIVE,
        payload,
    });
};

const setForceLoggedOutTokenExpire = (payload) => (dispatch) => {
    dispatch({
        type: SET_FORCE_LOGGED_OUT_TOKEN_EXPIRE,
        payload,
    });
};

export {
    openGlobalLoader,
    closeGlobalLoader,
    openMoreApp,
    setForceLoggedOutInactive,
    setForceLoggedOutTokenExpire
};
