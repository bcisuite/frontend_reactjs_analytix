const initialState = {
    globalLoaderState: false,
    moreApp: false,
    forceLoggedOutInactive: false,
    forceLoggedOutTokenExpire: false
};

export default initialState;
