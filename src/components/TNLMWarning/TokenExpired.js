import Box from "@mui/material/Box";
import Typography from '@mui/material/Typography';

import imgLogo from '../../assets/svg/analytix-logo-text.svg';
import imgExp from '../../assets/svg/illustration_expired.svg';

const styles = {
    text: {
        fontSize: '14px',
        color: '#454550',
        marginLeft: '5px',
        '& p': {
            color: '#808080',
            lineHeight: '30px'
        }
    },
    logo: {
        width: 200,
    },
    bullet: {
        width: 300,
        margin: 'auto'
    },
    title: {
        fontWeight: 600,
        fontSize: '24px',
        color: '#454550',
        marginBottom: '5px'
    }
};

const TNLMWarningTokenExpired = () => {
    return (
        <Box alignContent="center" alignItems="center" display="flex">
            <Box sx={styles.bullet}>
                <Box component="img" src={imgExp} alt="Unauthorized" sx={styles.bullet} />
            </Box>
            <Box flex={1}>
                <Box sx={{ marginLeft: '50px' }}>
                    <Box component="img" src={imgLogo} sx={styles.logo} alt="TNLM Logo" />
                    <Box sx={styles.text}>
                        <Typography variant="h1" sx={styles.title}>Sorry this page is expired</Typography>
                        <Typography paragraph={true} variant="body1">Please contact your Account Manager or BCI Team to get <br />
                            more details</Typography>
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}

export default TNLMWarningTokenExpired;