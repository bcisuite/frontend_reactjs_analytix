import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from '@mui/material/Typography';

import styleVar from '../../styles/variable';

import imgLogo from '../../assets/svg/analytix-logo-text.svg';
import imgLock from '../../assets/svg/illustration_sessionExpired.svg';

const styles = {
    container: {
        position: 'fixed',
        zIndex: 100000,
        width: '100vw',
        height: '100vh',
        top: 0,
        left: 0,
        backgroundColor: '#fff'
    },
    text: {
        fontSize: '14px',
        color: styleVar['black'],
        marginLeft: '5px',
        '& p': {
            color: '#808080',
            lineHeight: '30px'
        }
    },
    logo: {
        width: 200,
    },
    bullet: {
        width: 300,
        margin: 'auto'
    },
    title: {
        fontWeight: 600,
        fontSize: '24px',
        color: styleVar['black'],
        marginBottom: '5px'
    },
    btnSubmit: {
        borderRadius: 4,
        boxShadow: "none",
        backgroundColor: styleVar["red-bci"],
        color: "#fff",
        fontSize: "14px",
        fontWeight: "600",
        border: 0,
        padding: "7px 25px",
        "&:hover": {
            boxShadow: "none",
            backgroundColor: styleVar["red-bci"],
            color: "#fff",
        },
        marginTop: "20px",
        "&.Mui-disabled": {
            backgroundColor: `#d0d0d0 !important`,
            borderColor: `#d0d0d0 !important`,
        },
        '&.btnCancel': {
            backgroundColor: styleVar["gray"],
            "&:hover": {
                backgroundColor: styleVar["gray"],
            },
        },
        minWidth: "100px",
        textTransform: "none"
    },

    btnCancel: {
        borderRadius: 4,
        boxShadow: "none",
        backgroundColor: "transparent",
        color: styleVar["grey-text"],
        fontSize: "14px",
        fontWeight: "600",
        border: 0,
        padding: "7px 25px",
        "&:hover": {
            boxShadow: "none"
        },
        marginTop: "20px",
        minWidth: "100px",
        textTransform: "none"
    },
};

const TNLMWarningForceLoggedOutTokenExpired = ({ onHandleSubmit, show }) => {
    return (
        <Backdrop sx={styles.container} open={show}>
            <Box sx={styles.container} display="flex" alignItems="center" justifyContent="center">
                <Box alignContent="center" alignItems="center" display="flex">
                    <Box sx={styles.bullet}>
                        <Box component="img" src={imgLock} alt="Locked Illustration" sx={styles.bullet} />
                    </Box>
                    <Box flex={1}>
                        <Box sx={{ marginLeft: '50px' }}>
                            <Box component="img" src={imgLogo} sx={styles.logo} alt="TNLM Logo" />
                            <Box sx={styles.text}>
                                <Typography variant="h1" sx={styles.title}>Session Expired</Typography>
                                <Typography paragraph={true} variant="body1">Please make sure you aren't logged on another device</Typography>

                                <Button
                                    id="btn-submit"
                                    sx={styles.btnSubmit}
                                    variant="outlined"
                                    onClick={onHandleSubmit}
                                >
                                    Go To Login Page
                                </Button>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Box>
        </Backdrop>
    )
}

export default TNLMWarningForceLoggedOutTokenExpired;