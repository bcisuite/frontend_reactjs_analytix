import Box from "@mui/material/Box";
import Typography from '@mui/material/Typography';

import imgLogo from '../../assets/svg/analytix-logo-text.svg';
import imgDenied from '../../assets/svg/illustration_denided.svg';

const styles = {
    text: {
        fontSize: '14px',
        color: '#454550',
        marginLeft: '5px',
        '& p': {
            color: '#808080',
            lineHeight: '30px'
        }
    },
    logo: {
        width: 200,
    },
    bullet: {
        width: 300,
        margin: 'auto'
    },
    title: {
        fontWeight: 600,
        fontSize: '24px',
        color: '#454550',
        marginBottom: '5px'
    }
};

const TNLMWarningUnderConstruction = () => {
    return (
        <Box alignContent="center" alignItems="center" display="flex">
            <Box sx={styles.bullet}>
                <Box component="img" src={imgDenied} alt="Error" sx={styles.bullet} />
            </Box>
            <Box flex={1}>
                <Box sx={{ marginLeft: '50px' }}>
                    <Box component="img" src={imgLogo} sx={styles.logo} alt="TNLM Logo" />
                    <Box sx={styles.text}>
                        <Typography variant="h1" sx={styles.title}>Under Construction</Typography>
                        <Typography paragraph={true} variant="body1">Ooops, sorry this page is under construction</Typography>
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}

export default TNLMWarningUnderConstruction;