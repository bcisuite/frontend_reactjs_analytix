import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from '@mui/material/Typography';

import styleVar from '../../styles/variable';

import imgLogo from '../../assets/svg/analytix-logo-text.svg';
import imgActiveSub from '../../assets/svg/illustration_activeSub.svg';

const styles = {
    text: {
        fontSize: '14px',
        color: '#454550',
        marginLeft: '5px',
        '& p': {
            color: '#808080',
            lineHeight: '30px'
        }
    },
    logo: {
        width: 200,
    },
    bullet: {
        width: 300,
        margin: 'auto'
    },
    title: {
        fontWeight: 600,
        fontSize: '24px',
        color: '#454550',
        marginBottom: '5px'
    },
    btnCancel: {
        borderRadius: 4,
        boxShadow: "none",
        backgroundColor: styleVar["red-bci"],
        color: "#fff",
        fontSize: "14px",
        fontWeight: "600",
        border: 0,
        padding: "7px 25px",
        "&:hover": {
            boxShadow: "none",
            backgroundColor: styleVar["red-bci"],
            color: "#fff",
        },
        marginTop: "20px",
        marginLeft: "5px",
        "&.Mui-disabled": {
            backgroundColor: `#d0d0d0 !important`,
            borderColor: `#d0d0d0 !important`,
        },
        '&.btnCancel': {
            backgroundColor: styleVar["gray"],
            "&:hover": {
                backgroundColor: styleVar["gray"],
            },
        },
        minWidth: "100px",
        textTransform: "none"
    },
};

const TNLMWarningSubscriptionInactive = ({ onCancel }) => {
    return (
        <Box alignContent="center" alignItems="center" display="flex">
            <Box sx={styles.bullet}>
                <Box component="img" src={imgActiveSub} alt="No Active Subscription" sx={styles.bullet} />
            </Box>
            <Box flex={1}>
                <Box sx={{ marginLeft: '50px' }}>
                    <Box component="img" src={imgLogo} sx={styles.logo} alt="TNLM Logo" />
                    <Box sx={styles.text}>
                        <Typography variant="h1" sx={styles.title}>Your subscription is not active</Typography>
                        <Typography paragraph={true} variant="body1">Please contact your Account Manager or BCI Team to <br />
                            re-activate the subscription</Typography>
                    </Box>
                    <Button
                        id="btn-cancel"
                        sx={styles.btnCancel}
                        variant="outlined"
                        onClick={onCancel}
                    >
                        Go to Login Page
                    </Button>
                </Box>
            </Box>
        </Box>
    )
}

export default TNLMWarningSubscriptionInactive;