import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from '@mui/material/Typography';

import styleVar from '../../styles/variable';

import imgLogo from '../../assets/svg/analytix-logo-text.svg';
import imgLock from '../../assets/svg/illustration_lock.svg';

const styles = {
    text: {
        fontSize: '14px',
        marginLeft: '5px',
        '& p': {
            color: '#808080',
            lineHeight: '30px'
        }
    },
    logo: {
        width: 200,
    },
    bullet: {
        width: 300,
        margin: 'auto'
    },
    title: {
        fontWeight: 600,
        fontSize: '24px',
        marginBottom: '5px'
    },
    btnSubmit: {
        borderRadius: 4,
        boxShadow: "none",
        backgroundColor: styleVar["red-bci"],
        color: "#fff",
        fontSize: "14px",
        fontWeight: "600",
        border: 0,
        padding: "7px 25px",
        "&:hover": {
            boxShadow: "none",
            backgroundColor: styleVar["red-bci"],
            color: "#fff",
        },
        marginTop: "20px",
        marginLeft: "10px",
        "&.Mui-disabled": {
            backgroundColor: `#d0d0d0 !important`,
            borderColor: `#d0d0d0 !important`,
        },
        '&.btnCancel': {
            backgroundColor: styleVar["gray"],
            "&:hover": {
                backgroundColor: styleVar["gray"],
            },
        },
        minWidth: "100px",
        textTransform: "none"
    },

    btnCancel: {
        borderRadius: 4,
        boxShadow: "none",
        backgroundColor: "transparent",
        color: styleVar["grey-text"],
        fontSize: "14px",
        fontWeight: "600",
        border: 0,
        padding: "7px 25px",
        "&:hover": {
            boxShadow: "none"
        },
        marginTop: "20px",
        minWidth: "100px",
        textTransform: "none"
    },
};

const TNLMWarningAnotherLoginConfirmation = ({ onHandleSubmit, onCancel }) => {
    return (
        <Box alignContent="center" alignItems="center" display="flex">
            <Box sx={styles.bullet}>
                <Box component="img" src={imgLock} alt="Locked Illustration" sx={styles.bullet} />
            </Box>
            <Box flex={1}>
                <Box sx={{ marginLeft: '50px' }}>
                    <Box component="img" src={imgLogo} sx={styles.logo} alt="TNLM Logo" />
                    <Box sx={styles.text}>
                        <Typography variant="h1" sx={styles.title}>Multiple Logins Detected</Typography>
                        <Typography paragraph={true} variant="body1">Are you sure want to switch to this device?</Typography>

                        <Button
                            id="btn-cancel"
                            sx={styles.btnCancel}
                            variant="outlined"
                            onClick={onCancel}
                        >
                            Cancel
                        </Button>

                        <Button
                            id="btn-submit"
                            sx={styles.btnSubmit}
                            variant="outlined"
                            onClick={onHandleSubmit}
                        >
                            Switch to This Device
                        </Button>
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}

export default TNLMWarningAnotherLoginConfirmation;