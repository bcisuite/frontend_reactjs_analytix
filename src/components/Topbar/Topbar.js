import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Link, matchPath, useLocation } from "react-router-dom";
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';

import styleVar from "../../styles/variable";
import "./Topbar.scss";

import Routes from "../../Routes";

import { goLogout } from "../../services/authentication";

import TNLMIcon from "../../utils/TNLMIcon";

import TNLMPopoverCard from "../TNLMPopoverCard";

import ssoConfig from "../../config/sso.config";

const styles = {
    avaColor: {
        textTransform: "uppercase",
        color: styleVar["red-bci"],
        backgroundColor: styleVar["red-light"],
    },
    badge: {
        border: "2px solid #fff",
        fontWeight: "bold",
        padding: "0 8px",
        borderRadius: 10,
        color: "white",
        backgroundColor: "#0CD2D2",
    },
    seeAll: {
        cursor: "pointer",
        color: styleVar["red-bci"],
    },
    boxIcon: {
        width: "30px",
        height: "30px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    listItemAvatar: {
        width: 4.5,
        height: 4.5,
    },
    listItemAvatar_1: {
        backgroundColor: "#0CD2D2",
    },
    listItemAvatar_2: {
        backgroundColor: "#0039FF",
    },
    listItemAvatar_3: {
        backgroundColor: "#FF0000",
    },

};
const Topbar = () => {
    const [settingAnchorElm, setSettingAnchorElm] = useState();
    const {
        user: { userData, },
        layout,
    } = useSelector((state) => state);
    const location = useLocation();
    const [logout, setLogout] = useState(false);

    const [activeRoute, setActiveRoute] = useState({});

    const handleSettingOpen = (event) => {
        setSettingAnchorElm(event.currentTarget);
    };
    const handleSettingClose = () => {
        setSettingAnchorElm(null);
    };

    const getRouteData = (location) => {
        const activeRoute = Routes.find((x) =>
            matchPath(location?.pathname, {
                path: `${x.layout}${x.path}`,
                exact: true,
                strict: true,
            })
        );
        setActiveRoute(activeRoute);
    };

    const profileHeader = () => {
        return (
            <Box className="header">
                <span className="name">
                    {userData?.subscriber?.firstName
                        ? `${userData?.subscriber?.firstName || ""} ${userData?.subscriber?.lastName || ""}`
                        : userData?.subscriber?.username || ""}
                </span>
                <span className="title">
                    {userData?.subscriber?.flag === "FREE_SUBSCRIBERS" ||
                        userData?.subscriber?.flag === "FREE_SYD_SUBSCRIBERS"
                        ? "Job Title is not defined"
                        : userData?.subscriber?.title}
                </span>
            </Box>
        );
    };

    const profileFooter = () => {
        return (
            <Box className="footer">
                <span>
                    <b>Your Account Manager</b>
                </span>
                {userData?.subscriber?.flag === "SUBSCRIBERS" ||
                    userData?.subscriber?.flag === "SYD_SUBSCRIBERS" ? (
                    <span>
                        {userData?.subscriber?.accountManager}
                        <br />
                        {userData?.subscriber?.accountManagerEmail}
                    </span>
                ) : (
                    <span>To be appointed</span>
                )}
            </Box>
        );
    };

    const onLogout = () => {
        if (localStorage.getItem("accessToken")) {
            window.location.href = `${ssoConfig.tnlmBaseUri}/logout?app=${ssoConfig.tnlmAppName}`;
        } else {
            setLogout(true);
            goLogout({ subscriber_id: localStorage.subscriptionId })
                .then(() => {
                    if (localStorage.getItem("accessToken")) {
                        localStorage.clear();
                        window.location.href = `${ssoConfig.tnlmBaseUri}/logout?app=${ssoConfig.tnlmAppName}`;
                    } else {
                        localStorage.clear();
                        window.location.href = "/login";
                    }
                })
                .catch(
                    ({ data }) => data && data.error && console.log(data.error.message)
                )
                .finally(() => {
                    setLogout(false);
                });
        }
    };

    const showSetting = Boolean(settingAnchorElm);

    useEffect(() => {
        getRouteData(location);
    }, [location]);

    return (
        <Box
            id="main-topbar"
            className={`${activeRoute?.bigLayout ? "main-topbar-big" : ""} ${activeRoute?.homepage ? "main-homepage" : ""
                }`}
        >
            <Container className="top-content" maxWidth={false}>
                <Grid
                    style={{ height: "100%" }}
                    container
                    spacing={0}
                    direction="row"
                    justifyContent="space-between"
                    alignItems="center"
                >
                    <Grid style={{ height: "100%" }}>
                        {!activeRoute?.homepage && activeRoute?.title ? (
                            <Box className="nav-title" display="flex" flexDirection="column">
                                <Link to={`/main/${layout.PREV_VIEW}`}>
                                    <Typography
                                        className="back-text"
                                        color="textPrimary"
                                        variant="caption"
                                    >
                                        <TNLMIcon icon="arrow-left" size={1.5} />
                                        Back
                                    </Typography>
                                </Link>
                                <span className="title">{activeRoute?.title}</span>
                            </Box>
                        ) : (
                            <Box
                                className="nav-title"
                                display="flex"
                                alignItems="center"
                            ></Box>
                        )}
                    </Grid>
                    <Grid className="nav-list" style={{ height: "100%" }}>
                        <Box id="topbar-setting" className="nav-item">
                            <Box className="notification-icon" onClick={handleSettingOpen}>
                                <Avatar
                                    sx={styles.avaColor}
                                    variant="rounded"
                                    color="primary"
                                >
                                    {userData?.subscriber?.firstName
                                        ? `${userData?.subscriber?.firstName ? userData?.subscriber?.firstName[0] : ""}${userData?.subscriber?.lastName ? userData?.subscriber?.lastName[0] : ""
                                        }`
                                        : userData?.subscriber?.username
                                            ? userData?.subscriber?.username[0]
                                            : ""}
                                </Avatar>
                                <TNLMIcon icon="arrow-down" size={1.3} />
                            </Box>
                            <Popover
                                anchorEl={settingAnchorElm}
                                anchorOrigin={{
                                    horizontal: "left",
                                    vertical: "bottom",
                                }}
                                open={showSetting}
                                onClose={() => !logout && handleSettingClose()}
                            >
                                <TNLMPopoverCard
                                    id="setting-popover"
                                    CustomHeader={profileHeader}
                                    CustomFooter={profileFooter}
                                    onClose={handleSettingClose}
                                >
                                    {logout && (
                                        <Box id="topbarSettingLoad" className="topbarsettingLoad">
                                            <CircularProgress color="primary" />
                                        </Box>
                                    )}
                                    <Box className="body">
                                        <Box className="item" onClick={onLogout}>
                                            <Box className="icon">
                                                <TNLMIcon icon="sign-out" size={1.5} />
                                            </Box>
                                            <Box className="title">Logout</Box>
                                            <Box className="arrow">
                                                <TNLMIcon icon="arrow-right" size={1.5} />
                                            </Box>
                                        </Box>
                                    </Box>
                                </TNLMPopoverCard>
                            </Popover>
                        </Box>
                    </Grid>
                </Grid>
            </Container>
        </Box>
    );
};

export default Topbar;
