import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";

const styles = theme => ({
    backdrop: {
        zIndex: theme.zIndex.modal + 1,
        color: '#fff',
    },
});

const TNLMLoader = ({ open = false }) => {
    return (
        <Backdrop sx={styles.backdrop} open={open}>
            <CircularProgress color="inherit" />
        </Backdrop>
    )
}

export default TNLMLoader;