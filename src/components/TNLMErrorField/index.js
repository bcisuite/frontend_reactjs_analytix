import React from 'react';

import './_index.scss';

export default function TNLMErrorField({
    show = false,
    showBadge = true,
    children
}) {
    return show ? (
        <div>
            {showBadge && <div className="tnlm-error-field">
                <span className="tnlm-error-field-badge">i</span> &nbsp;&nbsp;{children}
            </div>}
            {!showBadge && <div className="tnlm-error-field">
                <span>{children}</span>
            </div>}
        </div>
    ) : null
}
