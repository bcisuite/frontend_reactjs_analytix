import React from "react";
import { useDispatch, useSelector } from "react-redux";

import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";

import styleVar from "../../styles/variable";

import imageArchify from "../../assets/svg/archify-logo-text.svg";
import imageLeadManager from "../../assets/svg/logo-with-text.svg";

import { openMoreApp } from "../../store/reducers/layout/action";

import TNLMIcon from "../../utils/TNLMIcon";

const styles = {
    root: {
        width: "316px",
    },
    container: {
        boxShadow: "none",
        padding: 3,
        display: "flex",
        flexDirection: "column",
    },
    appContainer: {
        border: `1px solid ${styleVar["grey-regular"]}`,
        boxSizing: "border-box",
        borderRadius: "8px",
        padding: 3,
        marginBottom: 3,
        cursor: "pointer",
    },
    appImage: {
        height: 20,
        marginBottom: 1,
    },
    appDescription: {
        color: styleVar["grey-text"],
    },
};

const data = [
    {
        id: 1,
        title: "Archify",
        url: "https://www.archify.com/",
        logo: imageArchify,
        appDescription:
            "Engage and network with design professionals on the all-in-one digital platform for design and specification",
    },
    {
        id: 2,
        title: "Lead Manager",
        url: 'https://www.bci-tnlm.com/',
        logo: imageLeadManager,
        appDescription:
            "Find, track and manage sales opportunities with live construction project data",
    },
];

export default function MoreApp() {
    const { moreApp } = useSelector((state) => state.layout);
    const dispatch = useDispatch();

    const onClose = () => dispatch(openMoreApp());

    return (
        <Drawer
            sx={{
                '& .MuiDrawer-paperAnchorLeft': styles.root,
            }}
            anchor={`left`}
            open={moreApp}
            onClose={onClose}
        >
            <Paper sx={styles.container}>
                <Box textAlign="right">
                    <IconButton onClick={onClose}>
                        <TNLMIcon icon="close" color={styleVar["grey-text"]} />
                    </IconButton>
                </Box>
                <Box mb={3}>
                    <Typography variant="caption">Switch to</Typography>
                </Box>
                <Box>
                    {data.map((app, index) => (
                        <Box
                            onClick={() => window.location.replace(app.url)}
                            key={index}
                            sx={styles.appContainer}
                        >
                            <Box component="img"
                                alt={app.title}
                                sx={styles.appImage}
                                src={app.logo}
                            />
                            <Typography sx={styles.appDescription} variant="body1">
                                {app.appDescription}
                            </Typography>
                        </Box>
                    ))}
                </Box>
            </Paper>
        </Drawer>
    );
}
