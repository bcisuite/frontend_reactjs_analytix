import { Link, useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import PropTypes from "prop-types";

import colorVar from "../../styles/variable";

import logo from "../../assets/logo-red.svg";

import { openMoreApp } from "../../store/reducers/layout/action";

import TNLMIcon from "../../utils/TNLMIcon";


import MoreApp from "./MoreApp";
import "./Sidebar.scss";

const sidebarMenuList = [
    {
        path: "home",
        icon: "read-book",
        title: "Home",
    },
];

const Sidebar = () => {

    const setting = { cleanView: false, };
    const dispatch = useDispatch();
    const location = useLocation();

    return (

        <div id="main-sidebar">
            {location.pathname.split("/")[2] !== "home" || !!setting?.cleanView === false ? (<Link to={`/main/home`}>
                <div className="sidebar-logo">
                    <img src={logo} alt="TNLM" title="TNLM" />
                </div>
            </Link>) : ""}

            <div className="sidebar-menu">
                {sidebarMenuList.map((val, idx) => {
                    return (
                        <Link key={idx} to={`/main/${val.path}`}>
                            <Tooltip
                                placement="right"
                                title={
                                    <Typography variant="caption">{val.title}</Typography>
                                }
                            >
                                <div className="menu" key={idx}>
                                    <TNLMIcon
                                        icon={val.icon}
                                        size={1.8}
                                        color={colorVar["trolley-gray"]}
                                    />
                                </div>
                            </Tooltip>
                        </Link>
                    );
                })}
            </div>
            <Tooltip
                placement="right"
                title={<Typography variant="caption">More App</Typography>}
            >
                <div className="other-app" onClick={() => dispatch(openMoreApp())}>
                    <TNLMIcon
                        icon="more-app"
                        size={1.8}
                        color={colorVar["trolley-gray"]}
                    />
                </div>
            </Tooltip>
            <MoreApp />
        </div>
    );
};

Sidebar.propTypes = {
    routes: PropTypes.array.isRequired,
};

export default Sidebar;
