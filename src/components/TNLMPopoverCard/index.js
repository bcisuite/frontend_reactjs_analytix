import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

import styleVar from "../../styles/variable";

import TNLMIcon from "../../utils/TNLMIcon";

const styles = {
    MuiPopoverCardHeader: {
        display: "flex",
        justifyContent: "space-between",
        padding: "12px",
        borderBottom: `1px solid ${styleVar["grey-regular"]}`,
        "& span": {
            color: styleVar["red-bci"],
            fontWeight: "600",
        },
        "& .close-btn": {
            cursor: "pointer",
            display: "flex",
            alignItems: "center",
        },
    },
    MuiPopoverCardBody: {
        height: "100%",
    },
    MuiPopoverCardFooter: {
        padding: "12px",
        textAlign: "right",
        borderTop: `1px solid ${styleVar["grey-regular"]}`,
    },
    btnCancel: {
        marginRight: "16px",
        boxShadow: "none",
        border: "none",
        marginLeft: "10px",
        fontSize: "14px",
        padding: "7px 20px",
        color: "#454550",
        "&:hover": {
            boxShadow: "none",
            backgroundColor: "#D6D6D6",
            border: "none",
        },
    },
    btnSubmit: {
        borderRadius: 4,
        boxShadow: "none",
        backgroundColor: styleVar["red-bci"],
        color: "#fff",
        fontSize: "14px",
        border: 0,
        padding: "7px 25px",
        "&:hover": {
            boxShadow: "none",
            backgroundColor: styleVar["red-bci"],
            color: "#fff",
        },
    },
};

const TNMLPopoverCard = ({
    children,
    id,
    className = "",
    title,
    onClose,
    onSubmit,
    btnSubmitText,
    CustomHeader,
    CustomFooter,
    showCancelBtn,
    btnCancelText,
    onCancel,
    btnSubmitClass,
    btnCancelClass,
    btnSubmitDisabled = false,
    ...other
}) => {
    return (
        <Box
            display="flex"
            flexDirection="column"
            wrap="nowrap"
            id={id}
            className={`${className}`}
            {...other}
        >
            {(title || CustomHeader) && (
                <Box>
                    <Box sx={styles.MuiPopoverCardHeader}>
                        {CustomHeader ? <CustomHeader /> : <span>{title}</span>}
                        <div className="close-btn" onClick={onClose}>
                            <TNLMIcon icon="close" />
                        </div>
                    </Box>
                </Box>
            )}
            <Box style={{ height: `calc(100% - 110px) ` }}>
                <Box sx={styles.MuiPopoverCardBody}>{children}</Box>
            </Box>
            {CustomFooter && (
                <Box>
                    <Box sx={styles.MuiPopoverCardFooter}>
                        <CustomFooter />
                    </Box>
                </Box>
            )}
            {!CustomFooter && onSubmit && (
                <Box overflow="hidden">
                    <Box sx={styles.MuiPopoverCardFooter}>
                        {showCancelBtn && (
                            <Button
                                id="btn-cancel"
                                className={`${btnCancelClass}`}
                                classes={{ '& .root': styles.btnCancel }}
                                variant="outlined"
                                color="secondary"
                                onClick={onCancel}
                            >
                                {btnCancelText || "Cancel"}
                            </Button>
                        )}

                        <Button
                            id="btn-submit"
                            className={`${btnSubmitClass || ""}`}
                            classes={{ '& .root': styles.btnSubmit }}
                            variant="outlined"
                            onClick={onSubmit}
                            disabled={btnSubmitDisabled}
                        >
                            {btnSubmitText}
                        </Button>
                    </Box>
                </Box>
            )}
        </Box>
    );
};

export default TNMLPopoverCard;
