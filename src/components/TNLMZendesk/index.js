import { Component } from "react";
import PropTypes from "prop-types";

import styleVar from "../../styles/variable";

import zendeskConfig from "../../config/zendesk.config";

// Take contact form as an example
// Let's customise our contact form appearance, launcher and add prefill content
const setting = {
    webWidget: {
        color: {
            // theme: styleVar['red-bci'], // let setup the theme from zendesk dashboard
            launcher: "#fff",
            launcherText: "#7d7d84",
            articleLinks: styleVar["blue"],
            resultLists: styleVar["blue"],
        },
        // to show popup properly
        zIndex: 1200,
    },
};

const canUseDOM = () => {
    if (
        typeof window === "undefined" ||
        !window.document ||
        !window.document.createElement
    ) {
        return false;
    }
    return true;
};

export const ZendeskAPI = (...args) => {
    if (canUseDOM && window.zE) {
        window.zE.apply(null, args);
    } else {
        console.warn("Zendesk is not initialized yet");
    }
};

var isLoaded = false;

export default class TNLMZendesk extends Component {
    constructor(props) {
        super(props);
        this.insertScript = this.insertScript.bind(this);
        this.onScriptLoaded = this.onScriptLoaded.bind(this);
    }

    onScriptLoaded() {
        if (typeof this.props.onLoaded === "function") {
            if (!isLoaded) {
                setTimeout(() => {
                    var iframe = document.getElementById("launcher");
                    var cssText =
                        ".u-userLauncherColor, .u-userLauncherColor:not([disabled]) {" +
                        "box-shadow: inset 0px 0px 0px 1px rgb(0 0 0 / 5%), 0px 0px 1px 0px rgb(0 0 0 / 11%) !important;" +
                        "}";
                    var css = document.createElement("style");
                    css.appendChild(document.createTextNode(cssText));

                    iframe?.contentWindow?.document
                        .querySelector("head")
                        .appendChild(css);
                }, 1000);

                this.props.onLoaded();
                isLoaded = true;
            }
        }
    }

    insertScript(defer) {
        const script = document.createElement("script");
        if (defer) {
            script.defer = true;
        } else {
            script.async = true;
        }
        script.id = "ze-snippet";
        script.src = `https://static.zdassets.com/ekr/snippet.js?key=${zendeskConfig.apiKey}`;
        script.addEventListener("load", this.onScriptLoaded);
        document.body.appendChild(script);
    }

    componentDidMount() {
        if (canUseDOM && !window.zE) {
            const { defer } = this.props;
            this.insertScript(defer);
            window.zESettings = setting;
        }
    }

    componentWillUnmount() {
        if (!canUseDOM || !window.zE) {
            return;
        }
        delete window.zE;
        delete window.zESettings;
    }

    render() {
        return null;
    }
}

TNLMZendesk.propTypes = {
    defer: PropTypes.bool,
};
