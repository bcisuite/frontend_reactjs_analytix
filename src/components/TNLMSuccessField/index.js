import React from 'react';
import './_index.scss';

export default function TNLMSuccessField({
    show = false,
    children
}) {
    return show ? (
        <div className="tnlm-success-field">
            {children}
        </div>
    ) : null
}
