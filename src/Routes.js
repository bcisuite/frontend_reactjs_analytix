import React from "react";

const Home = React.lazy(() => import("./pages/Home"));
const ProjectMilestones = React.lazy(() => import("./pages/ProjectMilestones"));

const routes = [
    {
        path: "/home",
        name: "Home",
        icon: "leaderboard",
        component: Home,
        layout: "/main",
        show: 1,
        title: "Home",
        bigLayout: false,
        homepage: true,
    },
    {
        path: "/project-milestones",
        name: "Project Milestones",
        icon: "search",
        component: ProjectMilestones,
        layout: "/main",
        show: 0,
        title: "Project Milestones",
        bigLayout: false,
    },
];

export default routes;
