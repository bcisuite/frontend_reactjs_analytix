import flatMap from 'lodash/flatMap';
import mapKeys from 'lodash/mapKeys';
import groupBy from 'lodash/groupBy';

const functions = {
  adjustRangeValueMax: (min, max) => min === max ? max + 1 : max,

  rowToparentChildren: (rows, childrenKey) => {
    const normalized = [];
    let currentParent = null;
    let currentChildren = [];
    rows.forEach(row => {
      if (currentParent === null || currentParent.id !== row.PARENT_ID) {
        if (currentParent) {
          currentParent[childrenKey] = currentChildren;
          currentChildren = [];
        }
        currentParent = {
          id: row.PARENT_ID,
          label: row.PARENT_LABEL,
        }
        if (row.PARENT_ABB) {
          currentParent.abbreviation = row.PARENT_ABB;
        }
        normalized.push(currentParent);
      }
      if (row.CHILD_ID) {
        currentChildren.push({
          id: row.CHILD_ID,
          label: row.CHILD_LABEL,
          parent_id: row.PARENT_ID,
          parent_label: row.PARENT_LABEL,
        });
      }
    });
    if (currentParent) {
      currentParent[childrenKey] = currentChildren;
    }
    return normalized;
  },

  formatNumberValue: (value) => {
    return value ? value.toLocaleString() : 0;
  },

  normalizePercent: (percent) => {
    return percent > 1000 ? 1001 : percent;
  },

  rowToLookupList: (rows, lookupListTypeKey) => {
    return groupBy(flatMap(rows, row => mapKeys(row, (value, key) => key.toLowerCase())), lookupListTypeKey);
  },
}

export default functions;