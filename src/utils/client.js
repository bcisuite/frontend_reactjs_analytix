import axios from "axios";
import { doRefreshToken } from "../services/authentication";

import reduxStore from "../store";
import {
  setForceLoggedOutInactive,
  setForceLoggedOutTokenExpire,
} from "../store/reducers/layout/action";
const { dispatch } = reduxStore;

const instance = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL_API,
});

instance.interceptors.request.use(
  (config) => {
    if (localStorage.accessToken && localStorage.accessToken !== "undefined") {
      config.headers["Authorization"] = `Bearer ${localStorage.accessToken}`;
    }
    if (
      localStorage.subscriptionId &&
      localStorage.subscriptionId !== "undefined"
    ) {
      config.headers["X-Subscriber-ID"] = localStorage.subscriptionId;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  function (response) {
    if (response.headers["x-new-token"]) {
      localStorage.setItem("accessToken", response.headers["x-new-token"]);
    }
    return response;
  },
  function (error) {
    const errorData = error?.response?.data;
    const originalRequest = error.config;
    // console.log(errorData);
    console.log(errorData);
    if (
      errorData?.error?.statusCode === 401 &&
      errorData?.error?.message === "ForceLogout"
    ) {
      //do logout because inactivity
      dispatch(setForceLoggedOutInactive(true));
    } else if (
      errorData?.error?.statusCode === 401 &&
      errorData?.error?.message === "RefreshTokenExpired"
    ) {
      //do logout because refresh token expire
      dispatch(setForceLoggedOutTokenExpire(true));
    } else if (
      errorData?.error?.statusCode === 401 &&
      errorData?.error?.message === "TokenExpired" &&
      !originalRequest._retry
    ) {
      //do refresh token and retry the request after got the new access token
      originalRequest._retry = true;

      return doRefreshToken({ refreshToken: localStorage.refreshToken }).then(
        (response) => {
          localStorage.setItem("accessToken", response.newAccessToken);
          originalRequest.headers["Authorization"] =
            "Bearer " + response.newAccessToken;
          return axios(originalRequest);
        },
        (error) => {
          const errorData = error?.data;

          if (errorData?.error?.statusCode === 401) {
            dispatch(setForceLoggedOutTokenExpire(true));
          }
          return Promise.reject(error);
        }
      );
    }
    return Promise.reject(error);
  }
);

export default instance;
