import React from 'react';
import PropTypes from 'prop-types';
import iconPath from './../assets/svgLib';

const defaultStyles = { display: 'flex', justifyContent: 'center', alignItems: 'center', minWidth: '100%' };

const TNLMIcon = ({ size, color, icon, className, style, viewBox, mask, maskOpacity, title }) => {
    const styles = { ...defaultStyles, ...style };
    return (
        <div title={title} style={{ width: `${size}em`, height: `${size}em`, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <svg
                className={className}
                style={styles}
                viewBox={viewBox}
                width={`${size}em`}
                height={`${size}em`}
                xmlns='http://www.w3.org/2000/svg'
                xmlnsXlink='http://www.w3.org/1999/xlink'
            >
                <path fill={color} d={iconPath[icon]} />
                {mask && <g xmlns="http://www.w3.org/2000/svg" mask="url(#mask0)">
                    <ellipse cx="6" cy="3.5" rx="15" ry="10.5" fill="white" fillOpacity={maskOpacity} />
                </g>}
            </svg>
        </div>
    );
};

TNLMIcon.defaultProps = {
    size: 1,
    color: '#000000',
    viewBox: '0 0 24 24',
    style: {},
    className: '',
    icon: '',
    mask: false,
    maskOpacity: '0.3'
};

TNLMIcon.propTypes = {
    size: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    viewBox: PropTypes.string.isRequired,
    style: PropTypes.object,
    className: PropTypes.string,
    mask: PropTypes.bool,
    maskOpacity: PropTypes.string
};

export default TNLMIcon;