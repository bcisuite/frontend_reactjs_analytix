import store from "../store";
const { userData } = store.getState().user;
const userRoleList = ["Regional Manager", "Manager", "Member"];
export const isMaster = userData.roleGroup === userRoleList[0];
export const isManager = userData.roleGroup === userRoleList[1];
export const isMember = userData.roleGroup === userRoleList[2];
export const isEmployee =
    userData.username.indexOf("emp_") === 0 ? true : false;
export const isProduction = process.env.REACT_APP_NODE_ENV === "PRODUCTION";
export const isDemo = process.env.REACT_APP_NODE_ENV === "DEMO";
export const isStaging = process.env.REACT_APP_NODE_ENV === "STAGING";
export const debounceTime = 500;
