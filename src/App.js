import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Redirect
} from 'react-router-dom';

import CommonRoute from "./routes/CommonRoute";
import PrivateRoute from "./routes/PrivateRoute";

import './App.scss';
import 'simplebar/dist/simplebar.min.css';

const MainLayout = React.lazy(() => import("./layout/MainLayout/MainLayout"));
const Callback = React.lazy(() => import("./pages/Auth/Callback"));

const App = () => {

    return (
        <Router>
            <React.Suspense>
                <Switch>
                    <CommonRoute path="/login/callback" component={Callback} />

                    <PrivateRoute path="/main" component={MainLayout} />

                    <Redirect from="/" to="/main/home" />
                </Switch>
            </React.Suspense>
        </Router>
    );
}

export default App;