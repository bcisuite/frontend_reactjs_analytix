import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActionArea from '@mui/material/CardActionArea';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

import projectMilestone from "../../assets/png/project-milestone.png";

import "./home.scss";

const Home = () => {
    const history = useHistory();

    return (
        <Box className="home-page" width="100%">
            <Typography variant="h1">BCI Dashboards</Typography>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={4}>
                    <Card>
                        <CardActionArea onClick={useCallback(() => history.push('/main/project-milestones'), [history])}>
                            <CardHeader title="Project Milestones" />
                            <CardMedia
                                component="img"
                                image={projectMilestone}
                                alt="green iguana"
                            />
                        </CardActionArea>
                    </Card>
                </Grid>
                <Grid item xs={12} sm={6} md={4}>
                    <Card>
                        <CardActionArea onClick={useCallback(() => history.push('/main/project-milestones'), [history])}>
                            <CardHeader title="Key Market Players" />
                            <CardMedia
                                component="img"
                                image={projectMilestone}
                                alt="green iguana"
                            />
                        </CardActionArea>
                    </Card>
                </Grid>
                <Grid item xs={12} sm={6} md={4}>
                    <Card>
                        <CardActionArea onClick={useCallback(() => history.push('/main/project-milestones'), [history])}>
                            <CardHeader title="Relationship Mapping" />
                            <CardMedia
                                component="img"
                                image={projectMilestone}
                                alt="green iguana"
                            />
                        </CardActionArea>
                    </Card>
                </Grid>
            </Grid>
        </Box>
    );
};

export default Home;
