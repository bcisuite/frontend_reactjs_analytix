import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import Box from "@mui/material/Box";
import Container from "@mui/material/Container";

import TNLMWarningUnauthorized from "../../components/TNLMWarning/Unauthorized";
import TNLMWarningSubscriptionInactive from "../../components/TNLMWarning/SubscriptionInactive";
import TNLMWarningUnknownError from "../../components/TNLMWarning/UnknownError";
import TNLMWarningAnotherLoginConfirmation from "../../components/TNLMWarning/AnotherLoginConfirmation";

import { goLoginSso } from "../../services/authentication";

import { setUserData } from "../../store/reducers/user/action";

import useQuery from "../../utils/useQuery";

import ssoConfig from "../../config/sso.config";

const Callback = () => {
    const dispatch = useDispatch();
    const query = useQuery();
    const [httpStatus, setHttpStatus] = useState(null);
    const [message, setMessage] = useState("Unknown Error");
    const [cognitoUsername, setCognitoUsername] = useState(null);

    useEffect(() => {
        if (query.get("source") === "sso") {
            if (query.get("auth_code")) {
                const params = {
                    authCode: query.get("auth_code"),
                    clientId: ssoConfig.clientId,
                    clientSecret: ssoConfig.clientSecret,
                    callbackUri: ssoConfig.callbackUri,
                };
                goLoginSso(params)
                    .then(({ statusCode, data }) => {
                        if (statusCode === 200) {
                            if (data.anotherLogin) {
                                setHttpStatus(999);
                            } else {
                                window.location.href = localStorage.redirect ? localStorage.redirect : "/main/home";
                            }
                            const subscriber = data.profile;
                            const subscription = data.subscription;
                            const profile = {
                                subscriber,
                                subscription,
                            };
                            setCognitoUsername(data.cognitoUsername);
                            localStorage.setItem("profile", JSON.stringify(profile));
                            localStorage.setItem("subscriptionId", subscriber?.id);
                            localStorage.setItem("accessToken", data.accessToken);
                            if (data.refreshToken) {
                                localStorage.setItem("refreshToken", data.refreshToken);
                            }
                            dispatch(setUserData({
                                payload: profile
                            }));
                        } else {
                            setHttpStatus(statusCode);
                            setMessage(data?.message);
                        }
                    })
                    .catch((err) => {
                        setHttpStatus(err?.status ?? 500);
                    });
            } else {
                setMessage("Bad parameter");
                setHttpStatus(500);
            }
        } else {
            setMessage("Bad parameter");
            setHttpStatus(500);
        }
    }, [dispatch, query]);

    const doLogoutSsoForCurrentSession = () => {
        localStorage.clear();
        window.location.href = `${ssoConfig.tnlmBaseUri}/logout?app=${ssoConfig.tnlmAppName}`;
    };

    const reloginSsoWithForceLogoutOnAnotherLogin = () => {
        setHttpStatus(null);
        const params = {
            authCode: query.get("auth_code"),
            clientId: ssoConfig.clientId,
            clientSecret: ssoConfig.clientSecret,
            callbackUri: ssoConfig.callbackUri,
            forceLogoutAnotherLogin: true,
            username: cognitoUsername,
        };
        goLoginSso(params)
            .then(({ statusCode, data }) => {
                if (statusCode !== 200) {
                    setHttpStatus(statusCode);
                }
            })
            .catch((err) => {
                setHttpStatus(err?.status ?? 500);
            });
    };

    return (
        <Container maxWidth={false} style={{ backgroundColor: "#fff" }}>
            <Box
                display="flex"
                alignItems="center"
                justifyContent="center"
                style={{ height: "100vh" }}
            >
                {httpStatus === null && (
                    <Box flex={1}>
                        Oops!
                    </Box>
                )}
                {httpStatus === 401 && (
                    <TNLMWarningUnauthorized
                        message={message}
                        onCancel={doLogoutSsoForCurrentSession}
                    />
                )}
                {httpStatus === 404 && (
                    <TNLMWarningSubscriptionInactive
                        onCancel={doLogoutSsoForCurrentSession}
                    />
                )}
                {httpStatus === 500 && (
                    <TNLMWarningUnknownError
                        message={message}
                        onCancel={doLogoutSsoForCurrentSession}
                    />
                )}
                {httpStatus === 999 && (
                    <TNLMWarningAnotherLoginConfirmation
                        onCancel={doLogoutSsoForCurrentSession}
                        onHandleSubmit={reloginSsoWithForceLogoutOnAnotherLogin}
                    />
                )}
            </Box>
        </Container>
    );
};

export default Callback;
