import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import LinearProgress from '@mui/material/LinearProgress';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Typography from '@mui/material/Typography';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import Slider from '@mui/material/Slider';
import Button from '@mui/material/Button';
import LoadingButton from '@mui/lab/LoadingButton';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import Grow from '@mui/material/Grow';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { Bar } from '@nivo/bar';
import dataUtils from "../../utils/dataUtils";

const ProjectMilestones = (props) => {
    const MILESTONE_DESCRIPTION = [
        'The design application has been submitted to relevant approval authority',
        'The design application has been approved by relevant approval authority',
        'The Main Contractor/Builder has been appointed to the project',
        'The Main Contractor/Builder has commenced the main works',
        'Main works/construction has been completed',
    ];
    const MILESTONE_DURATION_NAME = ['Application to Approval', 'Approval to Contract Awarded', 'Awarded to Construction Start', 'Construction Start to End'];
    const MILESTONE_DURATION_DESCRIPTION = [
        ' length of time between the submission of a design application & gaining approval to proceed',
        ' length of time between gaining design approval & appointing a Main Contractor/Builder',
        ' length of time between the Main Contractor/Builder being appointed and commencing main works',
        ' length of time from main works commencing until project completion',
    ];
    const apiUrl = `${process.env.REACT_APP_ANALYTIX_BASE_URI}/project-milestones`;

    const MilestoneDurationTooltip = styled(({ className, ...props }) => (
        <Tooltip {...props} classes={{ popper: className }} />
    ))(({ theme }) => ({
        [`& .${tooltipClasses.tooltip}`]: {
            maxWidth: 360,
            backgroundColor: '#FFFFFF',
            color: 'rgba(0, 0, 0, 0.87)',
            fontSize: 13,
            boxShadow: 'rgb(0 0 0 / 20%) 0px 3px 3px -2px, rgb(0 0 0 / 14%) 0px 3px 4px 0px, rgb(0 0 0 / 12%) 0px 1px 8px 0px',
        },
    }));

    const getMonthDay = (numberOfDays) => {
        const numberOfMonth = Math.floor(numberOfDays / 30);
        const monthOrMonths = numberOfMonth > 1 ? 'Months' : 'Month';
        const numberOfMonthDays = numberOfDays % 30;
        const dayOrDays = numberOfMonthDays > 1 ? 'Days' : 'Day';
        return `${numberOfMonth} ${monthOrMonths} ${numberOfMonthDays} ${dayOrDays}`;
    }

    const parseReports = React.useCallback(
        (result) => {
            const MILESTONE_NAME = ['Application Submitted', 'Application Approved', 'Main Contractor Awarded', 'Construction Commenced', 'Construction Completed'];
            const BAR_COLORS = [
                '#FF9F9F',
                '#6082FF',
                '#FF4040',
                '#13F1F1',
                '#808080',
                '#BFCDFF',
                '#9AF9F9',
                '#E8E8E8',
                '#E8C1A0',
                '#F1E15B',
            ];
            const reports = {
                overallStatistics: result.reports.overallStatistics,
            };

            const milestonesReachedPerYear = [];
            const milestonesReachedYears = []
            const milestoneReachedPerYearNumberKeys = [];
            const milestoneReachedPerYearPercentKeys = [];
            if (Array.isArray(result.reports.milestonesReachedPerYear) && result.reports.milestonesReachedPerYear.length > 0) {
                let currentMilestoneNumner = 1;
                let currentMilestoneYearIndex = 0;
                let currentMilestoneData = {
                    milestone: MILESTONE_NAME[currentMilestoneNumner - 1],
                };
                milestonesReachedPerYear.push(currentMilestoneData);
                result.reports.milestonesReachedPerYear.forEach(milestoneReachedYear => {
                    if (milestoneReachedYear.MILESTONE > currentMilestoneNumner) {
                        currentMilestoneNumner = milestoneReachedYear.MILESTONE;
                        currentMilestoneYearIndex = 0;
                        currentMilestoneData = {
                            milestone: MILESTONE_NAME[currentMilestoneNumner - 1],
                        };
                        milestonesReachedPerYear.push(currentMilestoneData);
                    }
                    const numberKey = `number-${currentMilestoneYearIndex}`;
                    const percentKey = `percent-${currentMilestoneYearIndex}`;
                    if (currentMilestoneNumner === 1) {
                        milestonesReachedYears.push(milestoneReachedYear.MILESTONE_YEAR);
                        milestoneReachedPerYearNumberKeys.push(numberKey);
                        milestoneReachedPerYearPercentKeys.push(percentKey);
                    }
                    currentMilestoneData[numberKey] = milestoneReachedYear.NUMBER_OF_PROJECTS;
                    currentMilestoneData[percentKey] = dataUtils.normalizePercent(milestoneReachedYear.CHANGE_PERCENTAGE);
                    currentMilestoneData[`numberOfProjectsBelow5m-${currentMilestoneYearIndex}`] = milestoneReachedYear.NUMBER_OF_PROJECTS_BELOW_5M;
                    currentMilestoneData[`numberOfProjectsBetween5mAnd30m-${currentMilestoneYearIndex}`] = milestoneReachedYear.NUMBER_OF_PROJECTS_BETWEEN_5M_AND_30M;
                    currentMilestoneData[`numberOfProjectsAbove30m-${currentMilestoneYearIndex}`] = milestoneReachedYear.NUMBER_OF_PROJECTS_ABOVE_30M;
                    currentMilestoneData[`totalValue-${currentMilestoneYearIndex}`] = milestoneReachedYear.TOTAL_VALUE;
                    currentMilestoneData[`color-${currentMilestoneYearIndex}`] = BAR_COLORS[currentMilestoneYearIndex];
                    currentMilestoneYearIndex++;
                });
            }
            reports.milestonesReachedPerYear = milestonesReachedPerYear;
            reports.milestonesReachedYears = milestonesReachedYears;
            reports.milestoneReachedPerYearNumberKeys = milestoneReachedPerYearNumberKeys;
            reports.milestoneReachedPerYearPercentKeys = milestoneReachedPerYearPercentKeys;

            const milestonesReachedPerRegion = [];
            const milestoneReachedPerRegionNumberThisYearKeys = [];
            const milestoneReachedPerRegionPercentThisYearKeys = [];
            const milestoneReachedPerRegionNumberLast365DaysKeys = [];
            const milestoneReachedPerRegionPercentLast365DaysKeys = [];
            if (Array.isArray(result.reports.milestonesReachedPerRegion) && result.reports.milestonesReachedPerRegion.length > 0) {
                let currentMilestoneNumner = 1;
                let currentMilestoneTimeframe = 1;
                let currentMilestoneStateorprovinceIndex = 0;
                let currentMilestoneData = {
                    milestone: MILESTONE_NAME[currentMilestoneNumner - 1],
                };
                milestonesReachedPerRegion.push(currentMilestoneData);
                result.reports.milestonesReachedPerRegion.forEach(milestoneReachedRegion => {
                    if (milestoneReachedRegion.MILESTONE > currentMilestoneNumner) {
                        currentMilestoneNumner = milestoneReachedRegion.MILESTONE;
                        currentMilestoneStateorprovinceIndex = 0;
                        currentMilestoneData = {
                            milestone: MILESTONE_NAME[currentMilestoneNumner - 1],
                        };
                        milestonesReachedPerRegion.push(currentMilestoneData);
                    }
                    if (currentMilestoneTimeframe !== milestoneReachedRegion.TIMEFRAME) {
                        currentMilestoneTimeframe = milestoneReachedRegion.TIMEFRAME;
                        currentMilestoneStateorprovinceIndex = 0;
                    }
                    const currentMilestoneRegion = milestoneReachedRegion.STATEORPROVINCE_ID;
                    const currentMilestoneRegionIndex = `${currentMilestoneTimeframe}-${currentMilestoneRegion}`;
                    const numberKey = `number-${currentMilestoneRegionIndex}`;
                    const percentKey = `percent-${currentMilestoneRegionIndex}`;
                    if (currentMilestoneNumner === 1) {
                        if (currentMilestoneTimeframe === 1) {
                            milestoneReachedPerRegionNumberThisYearKeys.push(numberKey);
                            milestoneReachedPerRegionPercentThisYearKeys.push(percentKey);
                        } else {
                            milestoneReachedPerRegionNumberLast365DaysKeys.push(numberKey);
                            milestoneReachedPerRegionPercentLast365DaysKeys.push(percentKey);
                        }
                    }
                    currentMilestoneData[numberKey] = milestoneReachedRegion.NUMBER_OF_PROJECTS;
                    currentMilestoneData[percentKey] = dataUtils.normalizePercent(milestoneReachedRegion.CHANGE_PERCENTAGE);
                    currentMilestoneData[`numberOfProjectsBelow5m-${currentMilestoneRegionIndex}`] = milestoneReachedRegion.NUMBER_OF_PROJECTS_BELOW_5M;
                    currentMilestoneData[`numberOfProjectsBetween5mAnd30m-${currentMilestoneRegionIndex}`] = milestoneReachedRegion.NUMBER_OF_PROJECTS_BETWEEN_5M_AND_30M;
                    currentMilestoneData[`numberOfProjectsAbove30m-${currentMilestoneRegionIndex}`] = milestoneReachedRegion.NUMBER_OF_PROJECTS_ABOVE_30M;
                    currentMilestoneData[`totalValue-${currentMilestoneRegionIndex}`] = milestoneReachedRegion.TOTAL_VALUE;
                    currentMilestoneData[`color-${currentMilestoneRegionIndex}`] = BAR_COLORS[currentMilestoneStateorprovinceIndex];
                    currentMilestoneStateorprovinceIndex++;
                });
            }
            reports.milestonesReachedPerRegion = milestonesReachedPerRegion;
            reports.milestoneReachedPerRegionNumberThisYearKeys = milestoneReachedPerRegionNumberThisYearKeys;
            reports.milestoneReachedPerRegionPercentThisYearKeys = milestoneReachedPerRegionPercentThisYearKeys;
            reports.milestoneReachedPerRegionNumberLast365DaysKeys = milestoneReachedPerRegionNumberLast365DaysKeys;
            reports.milestoneReachedPerRegionPercentLast365DaysKeys = milestoneReachedPerRegionPercentLast365DaysKeys;

            return reports;
        }, []);

    const [error, setError] = React.useState(null);
    const [isLoaded, setIsLoaded] = React.useState(false);
    const [backdropOpen, setBackdropOpen] = React.useState(false);
    const [medianOrAverage, setMedianOrAverage] = React.useState(['MEDIAN', 'Median']);
    const [numberOrPercentByYear, setNumberOrPercentByYear] = React.useState(['number', 'Number']);
    const [numberOrPercentByRegion, setNumberOrPercentByRegion] = React.useState(['number', 'Number']);
    const [timeframeByRegion, setTimeframeByRegion] = React.useState(1);
    const [filters, setFilters] = React.useState(null);
    const [reports, setReports] = React.useState(null);
    const [unfilteredReports, setUnfilteredReports] = React.useState(null);
    const [selectedStateorprovince, setSelectedStateorprovince] = React.useState([]);
    const [lgaOptions, setLgaOptions] = React.useState([]);
    const [selectedLga, setSelectedLga] = React.useState([]);
    const [selectedDevelopmentType, setSelectedDevelopmentType] = React.useState([]);
    const [selectedSector, setSelectedSector] = React.useState([]);
    const [subSectorOptions, setSubSectorOptions] = React.useState([]);
    const [selectedSubSector, setSelectedSubSector] = React.useState([]);
    const [selectedOwnershipType, setSelectedOwnershipType] = React.useState([]);
    const [selectedNumberOfResidentialUnits, setSelectedNumberOfResidentialUnits] = React.useState([]);
    const [selectedNumberOfStoreys, setSelectedNumberOfStoreys] = React.useState([]);
    const [selectedValueInLocalCurrency, setSelectedValueInLocalCurrency] = React.useState([]);
    const [applyingFilters, setApplyingFilters] = React.useState(false);
    const [alertDialogOpen, setAlertDialogOpen] = React.useState(false);
    const [alertDialogTitle, setAlertDialogTitle] = React.useState(null);
    const [alertDialogContent, setAlertDialogContent] = React.useState(null);

    React.useEffect(() => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                countryId: 12,
            })
        };

        fetch(apiUrl, requestOptions)
            .then(res => res.json())
            .then(
                (result) => {
                    const filters = dataUtils.rowToLookupList(result.filters.listValue, 'filter_type');
                    filters.numberOfResidentialUnits = {
                        min: result.filters.rangeValue[0].MIN_NUMBER_OF_RESIDENTIAL_UNITS,
                        max: dataUtils.adjustRangeValueMax(result.filters.rangeValue[0].MIN_NUMBER_OF_RESIDENTIAL_UNITS, result.filters.rangeValue[0].MAX_NUMBER_OF_RESIDENTIAL_UNITS),
                    };
                    filters.numberOfStoreys = {
                        min: result.filters.rangeValue[0].MIN_NUMBER_OF_STOREYS,
                        max: dataUtils.adjustRangeValueMax(result.filters.rangeValue[0].MIN_NUMBER_OF_STOREYS, result.filters.rangeValue[0].MAX_NUMBER_OF_STOREYS),
                    };
                    filters.valueInLocalCurrency = {
                        min: result.filters.rangeValue[0].MIN_VALUE_IN_LOCAL_CURRENCY,
                        max: dataUtils.adjustRangeValueMax(result.filters.rangeValue[0].MIN_VALUE_IN_LOCAL_CURRENCY, result.filters.rangeValue[0].MAX_VALUE_IN_LOCAL_CURRENCY),
                    };
                    filters.stateorprovince = dataUtils.rowToparentChildren(result.filters.stateorprovince, 'lgas');
                    filters.sector = dataUtils.rowToparentChildren(result.filters.sector, 'subSectors');

                    const stateorprovinces = {};
                    filters.stateorprovince.forEach(stateorprovince => {
                        stateorprovinces[stateorprovince.id] = {
                            label: stateorprovince.label,
                            abbreviation: stateorprovince.abbreviation,
                        };
                    });
                    filters.stateorprovinces = stateorprovinces;

                    const reports = parseReports(result);

                    setUnfilteredReports(reports);
                    setReports(reports);
                    setFilters(filters);
                    setSelectedNumberOfResidentialUnits([filters.numberOfResidentialUnits.min, filters.numberOfResidentialUnits.max]);
                    setSelectedNumberOfStoreys([filters.numberOfStoreys.min, filters.numberOfStoreys.max]);
                    setSelectedValueInLocalCurrency([filters.valueInLocalCurrency.min, filters.valueInLocalCurrency.max]);
                    setIsLoaded(true);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [props, apiUrl, parseReports]);

    if (Number.isNaN(props.countryId) || error) {
        return null;
    } else if (!isLoaded) {
        return (
            <Box sx={{ width: '100%' }}>
                <LinearProgress color="secondary" />
            </Box>
        );
    } else {
        return (
            <Box>
                <Accordion>
                    <AccordionSummary expandIcon={<ExpandMoreIcon />} id="filter-header">
                        <Typography variant="h6" component="h6">Filter</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
                                <Typography variant="subtitle1" component="div">
                                    State or Province
                                </Typography>
                                <Autocomplete
                                    multiple
                                    filterSelectedOptions
                                    id="filter-stateorprovince"
                                    options={filters.stateorprovince}
                                    value={selectedStateorprovince}
                                    onChange={(event, newValue) => {
                                        const newLgaOptions = newValue.map(e => e.lgas).reduce((previous, current) => previous.concat(current), []);
                                        const newLgaOptionIdSet = newLgaOptions.reduce((previous, current) => { previous[current.id] = null; return previous }, {});
                                        const newSelectedLga = selectedLga.filter(e => e.id in newLgaOptionIdSet);
                                        setSelectedStateorprovince(newValue);
                                        setLgaOptions(newLgaOptions);
                                        setSelectedLga(newSelectedLga);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            placeholder="Please pick a state or province"
                                        />
                                    )}
                                />
                            </Grid>
                            <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
                                <Typography variant="subtitle1" component="div">
                                    LGA
                                </Typography>
                                <Autocomplete
                                    multiple
                                    filterSelectedOptions
                                    id="filter-lga"
                                    options={lgaOptions}
                                    groupBy={(option) => option.parent_label}
                                    value={selectedLga}
                                    onChange={(event, newValue) => {
                                        setSelectedLga(newValue);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            placeholder="Please pick a LGA"
                                        />
                                    )}
                                />
                            </Grid>
                            <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
                                <Typography variant="subtitle1" component="div">
                                    Development Types
                                </Typography>
                                <Autocomplete
                                    multiple
                                    filterSelectedOptions
                                    id="filter-development-type"
                                    options={filters.developmentType}
                                    value={selectedDevelopmentType}
                                    onChange={(event, newValue) => {
                                        setSelectedDevelopmentType(newValue);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            placeholder="Please pick a development type"
                                        />
                                    )}
                                />
                            </Grid>
                            <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
                                <Typography variant="subtitle1" component="div">
                                    Sector
                                </Typography>
                                <Autocomplete
                                    multiple
                                    filterSelectedOptions
                                    id="filter-sector"
                                    options={filters.sector}
                                    value={selectedSector}
                                    onChange={(event, newValue) => {
                                        const newSubSectorOptions = newValue.map(e => e.subSectors).reduce((previous, current) => previous.concat(current), []);
                                        const newSubSectorOptionIdSet = newSubSectorOptions.reduce((previous, current) => { previous[current.id] = null; return previous }, {});
                                        const newSelectedSubSector = selectedSubSector.filter(e => e.id in newSubSectorOptionIdSet);
                                        setSelectedSector(newValue);
                                        setSubSectorOptions(newSubSectorOptions);
                                        setSelectedSubSector(newSelectedSubSector);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            placeholder="Please pick a sector"
                                        />
                                    )}
                                />
                            </Grid>
                            <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
                                <Typography variant="subtitle1" component="div">
                                    Sub-Sectors
                                </Typography>
                                <Autocomplete
                                    multiple
                                    filterSelectedOptions
                                    id="filter-sub-sector"
                                    options={subSectorOptions}
                                    groupBy={(option) => option.parent_label}
                                    value={selectedSubSector}
                                    onChange={(event, newValue) => {
                                        setSelectedSubSector(newValue);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            placeholder="Please pick a sub-sector"
                                        />
                                    )}
                                />
                            </Grid>
                            <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
                                <Typography variant="subtitle1" component="div">
                                    Ownership Types
                                </Typography>
                                <Autocomplete
                                    multiple
                                    filterSelectedOptions
                                    id="filter-ownership-type"
                                    options={filters.ownershipType}
                                    value={selectedOwnershipType}
                                    onChange={(event, newValue) => {
                                        setSelectedOwnershipType(newValue);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            placeholder="Please pick a ownership type"
                                        />
                                    )}
                                />
                            </Grid>
                            <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
                                <Typography variant="subtitle1" component="div">
                                    Project Value in Local Currency
                                </Typography>
                                <Stack
                                    direction="row"
                                    alignItems="flex-end"
                                    justifyContent="center"
                                    spacing={2}
                                    sx={{ mt: 2, }}
                                >
                                    <TextField
                                        type="number"
                                        value={selectedValueInLocalCurrency[0]}
                                        size="medium"
                                        variant="standard"
                                        onChange={(event) => {
                                            const newMinValueInLocalCurrency = parseInt(event.target.value);
                                            if (!isNaN(newMinValueInLocalCurrency)) {
                                                setSelectedValueInLocalCurrency([newMinValueInLocalCurrency, selectedValueInLocalCurrency[1]]);
                                            }
                                        }}
                                        onBlur={() => {
                                            const newMinValueInLocalCurrency = Math.min(Math.max(filters.valueInLocalCurrency.min, selectedValueInLocalCurrency[0]), selectedValueInLocalCurrency[1]);
                                            const newMaxValueInLocalCurrency = Math.min(filters.valueInLocalCurrency.max, selectedValueInLocalCurrency[1]);
                                            setSelectedValueInLocalCurrency([newMinValueInLocalCurrency, newMaxValueInLocalCurrency]);
                                        }}
                                        inputProps={{
                                            step: 1,
                                            min: filters.valueInLocalCurrency.min,
                                            max: filters.valueInLocalCurrency.max,
                                        }}
                                        sx={{ minWidth: 60, }}
                                    />
                                    <Slider
                                        id="filter-value-in-local-currency"
                                        min={filters.valueInLocalCurrency.min}
                                        max={filters.valueInLocalCurrency.max}
                                        step={1}
                                        value={selectedValueInLocalCurrency}
                                        size="medium"
                                        onChange={(event, newValue) => {
                                            setSelectedValueInLocalCurrency(newValue);
                                        }}
                                        valueLabelDisplay="off"
                                        disableSwap
                                    />
                                    <TextField
                                        type="number"
                                        value={selectedValueInLocalCurrency[1]}
                                        size="medium"
                                        variant="standard"
                                        onChange={(event) => {
                                            const newMaxValueInLocalCurrency = parseInt(event.target.value);
                                            if (!isNaN(newMaxValueInLocalCurrency)) {
                                                setSelectedValueInLocalCurrency([selectedValueInLocalCurrency[0], newMaxValueInLocalCurrency]);
                                            }
                                        }}
                                        onBlur={() => {
                                            const newMinValueInLocalCurrency = Math.max(filters.valueInLocalCurrency.min, selectedValueInLocalCurrency[0]);
                                            const newMaxValueInLocalCurrency = Math.max(Math.min(filters.valueInLocalCurrency.max, selectedValueInLocalCurrency[1]), selectedValueInLocalCurrency[0]);
                                            setSelectedValueInLocalCurrency([newMinValueInLocalCurrency, newMaxValueInLocalCurrency]);
                                        }}
                                        inputProps={{
                                            step: 1,
                                            min: filters.valueInLocalCurrency.min,
                                            max: filters.valueInLocalCurrency.max,
                                        }}
                                        sx={{ minWidth: 60, }}
                                    />
                                </Stack>
                            </Grid>
                            <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
                                <Typography variant="subtitle1" component="div">
                                    Number Of Storeys
                                </Typography>
                                <Stack
                                    direction="row"
                                    alignItems="flex-end"
                                    justifyContent="center"
                                    spacing={2}
                                    sx={{ mt: 2, }}
                                >
                                    <TextField
                                        type="number"
                                        value={selectedNumberOfStoreys[0]}
                                        size="medium"
                                        variant="standard"
                                        onChange={(event) => {
                                            const newMinNumberOfStoreys = parseInt(event.target.value);
                                            if (!isNaN(newMinNumberOfStoreys)) {
                                                setSelectedNumberOfStoreys([newMinNumberOfStoreys, selectedNumberOfStoreys[1]]);
                                            }
                                        }}
                                        onBlur={() => {
                                            const newMinNumberOfStoreys = Math.min(Math.max(filters.numberOfStoreys.min, selectedNumberOfStoreys[0]), selectedNumberOfStoreys[1]);
                                            const newMaxNumberOfStoreys = Math.min(filters.numberOfStoreys.max, selectedNumberOfStoreys[1]);
                                            setSelectedNumberOfStoreys([newMinNumberOfStoreys, newMaxNumberOfStoreys]);
                                        }}
                                        inputProps={{
                                            step: 1,
                                            min: filters.numberOfStoreys.min,
                                            max: filters.numberOfStoreys.max,
                                        }}
                                        sx={{ minWidth: 60, }}
                                    />
                                    <Slider
                                        id="filter-value-in-local-currency"
                                        min={filters.numberOfStoreys.min}
                                        max={filters.numberOfStoreys.max}
                                        step={1}
                                        value={selectedNumberOfStoreys}
                                        size="medium"
                                        onChange={(event, newValue) => {
                                            setSelectedNumberOfStoreys(newValue);
                                        }}
                                        valueLabelDisplay="off"
                                        disableSwap
                                    />
                                    <TextField
                                        type="number"
                                        value={selectedNumberOfStoreys[1]}
                                        size="medium"
                                        variant="standard"
                                        onChange={(event) => {
                                            const newMaxNumberOfStoreys = parseInt(event.target.value);
                                            if (!isNaN(newMaxNumberOfStoreys)) {
                                                setSelectedNumberOfStoreys([selectedNumberOfStoreys[0], newMaxNumberOfStoreys]);
                                            }
                                        }}
                                        onBlur={() => {
                                            const newMinNumberOfStoreys = Math.max(filters.numberOfStoreys.min, selectedNumberOfStoreys[0]);
                                            const newMaxNumberOfStoreys = Math.max(Math.min(filters.numberOfStoreys.max, selectedNumberOfStoreys[1]), selectedNumberOfStoreys[0]);
                                            setSelectedNumberOfStoreys([newMinNumberOfStoreys, newMaxNumberOfStoreys]);
                                        }}
                                        inputProps={{
                                            step: 1,
                                            min: filters.numberOfStoreys.min,
                                            max: filters.numberOfStoreys.max,
                                        }}
                                        sx={{ minWidth: 60, }}
                                    />
                                </Stack>
                            </Grid>
                            <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
                                <Typography variant="subtitle1" component="div">
                                    Number Of Residential Units
                                </Typography>
                                <Stack
                                    direction="row"
                                    alignItems="flex-end"
                                    justifyContent="center"
                                    spacing={2}
                                    sx={{ mt: 2, }}
                                >
                                    <TextField
                                        type="number"
                                        value={selectedNumberOfResidentialUnits[0]}
                                        size="medium"
                                        variant="standard"
                                        onChange={(event) => {
                                            const newMinNumberOfResidentialUnits = parseInt(event.target.value);
                                            if (!isNaN(newMinNumberOfResidentialUnits)) {
                                                setSelectedNumberOfResidentialUnits([newMinNumberOfResidentialUnits, selectedNumberOfResidentialUnits[1]]);
                                            }
                                        }}
                                        onBlur={() => {
                                            const newMinNumberOfResidentialUnits = Math.min(Math.max(filters.numberOfResidentialUnits.min, selectedNumberOfResidentialUnits[0]), selectedNumberOfResidentialUnits[1]);
                                            const newMaxNumberOfResidentialUnits = Math.min(filters.numberOfResidentialUnits.max, selectedNumberOfResidentialUnits[1]);
                                            setSelectedNumberOfResidentialUnits([newMinNumberOfResidentialUnits, newMaxNumberOfResidentialUnits]);
                                        }}
                                        inputProps={{
                                            step: 1,
                                            min: filters.numberOfResidentialUnits.min,
                                            max: filters.numberOfResidentialUnits.max,
                                        }}
                                        sx={{ minWidth: 60, }}
                                    />
                                    <Slider
                                        id="filter-value-in-local-currency"
                                        min={filters.numberOfResidentialUnits.min}
                                        max={filters.numberOfResidentialUnits.max}
                                        step={1}
                                        value={selectedNumberOfResidentialUnits}
                                        size="medium"
                                        onChange={(event, newValue) => {
                                            setSelectedNumberOfResidentialUnits(newValue);
                                        }}
                                        valueLabelDisplay="off"
                                        disableSwap
                                    />
                                    <TextField
                                        type="number"
                                        value={selectedNumberOfResidentialUnits[1]}
                                        size="medium"
                                        variant="standard"
                                        onChange={(event) => {
                                            const newMaxNumberOfResidentialUnits = parseInt(event.target.value);
                                            if (!isNaN(newMaxNumberOfResidentialUnits)) {
                                                setSelectedNumberOfResidentialUnits([selectedNumberOfResidentialUnits[0], newMaxNumberOfResidentialUnits]);
                                            }
                                        }}
                                        onBlur={() => {
                                            const newMinNumberOfResidentialUnits = Math.max(filters.numberOfResidentialUnits.min, selectedNumberOfResidentialUnits[0]);
                                            const newMaxNumberOfResidentialUnits = Math.max(Math.min(filters.numberOfResidentialUnits.max, selectedNumberOfResidentialUnits[1]), selectedNumberOfResidentialUnits[0]);
                                            setSelectedNumberOfResidentialUnits([newMinNumberOfResidentialUnits, newMaxNumberOfResidentialUnits]);
                                        }}
                                        inputProps={{
                                            step: 1,
                                            min: filters.numberOfResidentialUnits.min,
                                            max: filters.numberOfResidentialUnits.max,
                                        }}
                                        sx={{ minWidth: 60, }}
                                    />
                                </Stack>
                            </Grid>
                            <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                                <Stack direction="row" justifyContent="flex-end" alignItems="center" spacing={2}>
                                    <Button
                                        variant="text"
                                        size="large"
                                        sx={{ textTransform: 'capitalize', color: '#6A6A73' }}
                                        onClick={() => {
                                            setSelectedStateorprovince([]);
                                            setSelectedLga([]);
                                            setSelectedDevelopmentType([]);
                                            setSelectedSector([]);
                                            setSelectedSubSector([]);
                                            setSelectedOwnershipType([]);
                                            setSelectedNumberOfResidentialUnits([filters.numberOfResidentialUnits.min, filters.numberOfResidentialUnits.max]);
                                            setSelectedNumberOfStoreys([filters.numberOfStoreys.min, filters.numberOfStoreys.max]);
                                            setSelectedValueInLocalCurrency([filters.valueInLocalCurrency.min, filters.valueInLocalCurrency.max]);
                                            setReports(unfilteredReports);
                                        }}
                                    >
                                        Clear Filters
                                    </Button>
                                    <LoadingButton
                                        loading={applyingFilters}
                                        loadingIndicator="Applying..."
                                        variant="contained"
                                        size="large"
                                        sx={{ textTransform: 'capitalize', }}
                                        onClick={async () => {
                                            if (selectedStateorprovince.length === 0 && selectedLga.length === 0 && selectedDevelopmentType.length === 0 &&
                                                selectedSector.length === 0 && selectedSubSector.length === 0 && selectedOwnershipType.length === 0 &&
                                                selectedNumberOfResidentialUnits[0] === filters.numberOfResidentialUnits.min &&
                                                selectedNumberOfResidentialUnits[1] === filters.numberOfResidentialUnits.max &&
                                                selectedNumberOfStoreys[0] === filters.numberOfStoreys.min &&
                                                selectedNumberOfStoreys[1] === filters.numberOfStoreys.max &&
                                                selectedValueInLocalCurrency[0] === filters.valueInLocalCurrency.min &&
                                                selectedValueInLocalCurrency[1] === filters.valueInLocalCurrency.max) {
                                                setAlertDialogTitle('Warning');
                                                setAlertDialogContent('No filter has been specified.');
                                                setAlertDialogOpen(true);
                                                return;
                                            }
                                            setBackdropOpen(true);
                                            setApplyingFilters(true);
                                            const sectorWithSelectedSubSectorIdSet = selectedSubSector.reduce((previous, current) => { previous[current.parent_id] = null; return previous }, {});
                                            const sectorsWithoutSelectedSubSector = selectedSector.filter(e => !(e.id in sectorWithSelectedSubSectorIdSet));
                                            const stateorprovincesWithSelectedLgaIdSet = selectedLga.reduce((previous, current) => { previous[current.parent_id] = null; return previous }, {});
                                            const stateorprovincesWithoutSelectedLga = selectedStateorprovince.filter(e => !(e.id in stateorprovincesWithSelectedLgaIdSet));
                                            const requestOptions = {
                                                method: 'POST',
                                                headers: { 'Content-Type': 'application/json' },
                                                body: JSON.stringify({
                                                    countryId: props.countryId,
                                                    filters: {
                                                        stateorprovinces: stateorprovincesWithoutSelectedLga.map(e => e.id),
                                                        lgas: selectedLga.map(e => e.id),
                                                        developmentTypes: selectedDevelopmentType.map(e => e.id),
                                                        subSectors: sectorsWithoutSelectedSubSector.reduce((previous, current) => { return previous.concat(current.subSectors) }, []).map(e => e.id).concat(selectedSubSector.map(e => e.id)),
                                                        ownershipTypes: selectedOwnershipType.map(e => e.id),
                                                        minNumberOfResidentialUnits: selectedNumberOfResidentialUnits[0],
                                                        maxNumberOfResidentialUnits: selectedNumberOfResidentialUnits[1],
                                                        minNumberOfStoreys: selectedNumberOfStoreys[0],
                                                        maxNumberOfStoreys: selectedNumberOfStoreys[1],
                                                        minValueInLocalCurrency: selectedValueInLocalCurrency[0],
                                                        maxValueInLocalCurrency: selectedValueInLocalCurrency[1],
                                                    },
                                                }),
                                            };
                                            try {
                                                const response = await fetch(apiUrl, requestOptions);
                                                const result = await response.json();
                                                const reports = parseReports(result);

                                                setReports(reports);
                                                setApplyingFilters(false);
                                                setBackdropOpen(false);
                                            } catch (e) {
                                                setApplyingFilters(false);
                                                setBackdropOpen(false);
                                            }
                                        }}
                                    >
                                        Apply Filters
                                    </LoadingButton>
                                </Stack>
                            </Grid>
                        </Grid>
                    </AccordionDetails>
                </Accordion>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={12} lg={12} xl={4}>
                        <Stack direction="column" spacing={2}>
                            <Stack direction="row" justifyContent="center" alignItems="center" spacing={2}>
                                <Typography variant="h6" component="h6">Overall Statistics over Last 5 Years</Typography>
                                <FormControl component="fieldset">
                                    <RadioGroup row name="median-or-average" value={medianOrAverage[1]} onChange={(event) => { setMedianOrAverage([event.target.value.toUpperCase(), event.target.value]); }}>
                                        <FormControlLabel value="Median" control={<Radio />} label="Median" />
                                        <FormControlLabel value="Average" control={<Radio />} label="Average" />
                                    </RadioGroup>
                                </FormControl>
                            </Stack>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Milestone</TableCell>
                                        <TableCell>Abandoned Rate</TableCell>
                                        <TableCell>Duration</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {reports.overallStatistics.map((row) => (
                                        <TableRow key={row.MILESTONE}>
                                            <TableCell align="left">{MILESTONE_DURATION_NAME[row.MILESTONE - 1]}</TableCell>
                                            <TableCell align="left">{Math.round(row.ABANDONED_RATE * 100) / 100}%</TableCell>
                                            <TableCell align="left">
                                                <MilestoneDurationTooltip
                                                    title={`${medianOrAverage[1]}${MILESTONE_DURATION_DESCRIPTION[row.MILESTONE - 1]}`}
                                                    placement="top"
                                                    transitioncomponent={Grow}
                                                    transitionprops={{ timeout: 100, }}
                                                >
                                                    <Typography variant="body2">{getMonthDay(row[`${medianOrAverage[0]}_DURATION`])}</Typography>
                                                </MilestoneDurationTooltip>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                            <Stack direction="column" justifyContent="center" alignItems="center" spacing={1}>
                                <Typography variant="subtitle1">{medianOrAverage[1]} time between development approval and construction start</Typography>
                                <Typography variant="h6" component="h6">{getMonthDay(reports.overallStatistics[0][`${medianOrAverage[0]}_MILESTONE_2_4_DURATION`])}</Typography>
                            </Stack>
                            <Stack direction="column" justifyContent="center" alignItems="center" spacing={1}>
                                <Typography variant="subtitle1">{medianOrAverage[1]} project value</Typography>
                                <Typography variant="h6" component="h6">${Math.round(reports.overallStatistics[0][`${medianOrAverage[0]}_VALUE_IN_LOCAL_CURRENCY`] * 1000) / 1000}M</Typography>
                            </Stack>
                        </Stack>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12} xl={8}>
                        <Stack direction="column" spacing={2}>
                            <Stack direction="row" justifyContent="left" alignItems="center" spacing={2}>
                                <Typography variant="h6" component="h6">Milestone Reached per Year</Typography>
                                <FormControl component="fieldset">
                                    <RadioGroup row name="number-or-percent-year" value={numberOrPercentByYear[1]} onChange={(event) => { setNumberOrPercentByYear([event.target.value.toLowerCase(), event.target.value]); }}>
                                        <FormControlLabel value="Number" control={<Radio />} label="Number of Projects" />
                                        <FormControlLabel value="Percent" control={<Radio />} label="Percent Change from Previous Year" />
                                    </RadioGroup>
                                </FormControl>
                            </Stack>
                            <Bar
                                data={reports.milestonesReachedPerYear}
                                indexBy="milestone"
                                keys={reports[`milestoneReachedPerYear${numberOrPercentByYear[1]}Keys`]}
                                layout="vertical"
                                groupMode="grouped"
                                valueScale={{ type: 'linear' }}
                                indexScale={{ type: 'band', round: true }}
                                maxValue={numberOrPercentByYear[0] === 'number' ? 15000 : 50}
                                enableLabel={true}
                                label={(data) => {
                                    if (numberOrPercentByYear[0] === 'number') {
                                        if (data.value < 9) {
                                            return <tspan y="-10">{data.value}</tspan>;
                                        }
                                    } else {
                                        if (data.value === 0) {
                                            return <tspan y="-10">0</tspan>;
                                        }
                                    }
                                    return null;
                                }}
                                axisTop={null}
                                axisRight={null}
                                axisLeft={{
                                    format: " >-,",
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: numberOrPercentByYear[0] === 'number' ? 'Number of Projects' : '% Change from Previous Year',
                                    legendPosition: 'middle',
                                    legendOffset: -60,
                                }}
                                colors={({ id, data }) => String(data[`color-${id.split('-')[1]}`])}
                                width={1000}
                                height={400}
                                padding={0.3}
                                margin={{ top: 60, right: 0, bottom: 80, left: 80, }}
                                legendLabel={datum => `${reports.milestonesReachedYears[parseInt(datum.id.split('-')[1])]}`}
                                legends={[
                                    {
                                        dataFrom: 'keys',
                                        anchor: 'bottom-left',
                                        direction: 'row',
                                        justify: false,
                                        itemWidth: 80,
                                        itemHeight: 20,
                                        translateX: -40,
                                        translateY: 80,
                                    },
                                ]}
                                tooltip={({ id, value, index, indexValue, data }) => {
                                    const yearIndex = parseInt(id.split('-')[1]);
                                    const milestoneIndex = index;
                                    let formattedValue = null;
                                    if (numberOrPercentByYear[0] === 'number') {
                                        formattedValue = dataUtils.formatNumberValue(value);
                                    } else {
                                        if (value > 1000) {
                                            formattedValue = '> 1,000%';
                                        } else {
                                            formattedValue = `${dataUtils.formatNumberValue(Math.round(value * 100) / 100)}%`;
                                        }
                                    }
                                    return (
                                        <div
                                            style={{
                                                padding: 10,
                                                boxShadow: 'rgb(0 0 0 / 20%) 0px 3px 3px -2px, rgb(0 0 0 / 14%) 0px 3px 4px 0px, rgb(0 0 0 / 12%) 0px 1px 8px 0px',
                                                backgroundColor: '#FFFFFF',
                                                fontSize: 13
                                            }}
                                        >
                                            <table style={{ width: '100%', marginBottom: 20, }}>
                                                <tbody>
                                                    <tr>
                                                        <td>Period:</td>
                                                        <td style={{ fontWeight: 'bold', }}>{reports.milestonesReachedYears[yearIndex]}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Milestone:</td>
                                                        <td style={{ fontWeight: 'bold', }}>{indexValue}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Description:</td>
                                                        <td>{MILESTONE_DESCRIPTION[milestoneIndex]}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table style={{ width: '100%', }}>
                                                <tbody>
                                                    <tr>
                                                        <td>{numberOrPercentByYear[0] === 'number' ? 'Total Number of Projects' : '% Change from Previous Year'}:</td>
                                                        <td style={{ fontWeight: 'bold', }}>{formattedValue}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Number of Projects with Value Below 5M:</td>
                                                        <td style={{ fontWeight: 'bold', }}>{dataUtils.formatNumberValue(data[`numberOfProjectsBelow5m-${yearIndex}`])}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Number of Projects with Value Between 5M and 30M:</td>
                                                        <td style={{ fontWeight: 'bold', }}>{dataUtils.formatNumberValue(data[`numberOfProjectsBetween5mAnd30m-${yearIndex}`])}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Number of Projects with Value Above 30M:</td>
                                                        <td style={{ fontWeight: 'bold', }}>{dataUtils.formatNumberValue(data[`numberOfProjectsAbove30m-${yearIndex}`])}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total Project Value:</td>
                                                        <td style={{ fontWeight: 'bold', }}>${dataUtils.formatNumberValue(data[`totalValue-${yearIndex}`])}M</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    );
                                }}
                                animate={true}
                                isInteractive={true}
                            />
                            <Stack direction="row" justifyContent="left" alignItems="center" spacing={2}>
                                <Typography variant="h6" component="h6">Milestone Reached per Region</Typography>
                                <FormControl component="fieldset">
                                    <RadioGroup row name="number-or-percent-region" value={numberOrPercentByRegion[1]} onChange={(event) => { setNumberOrPercentByRegion([event.target.value.toLowerCase(), event.target.value]); }}>
                                        <FormControlLabel value="Number" control={<Radio />} label="Number of Projects" />
                                        <FormControlLabel value="Percent" control={<Radio />} label="Percent Change from Previous Year" />
                                    </RadioGroup>
                                </FormControl>
                                <FormControl component="fieldset">
                                    <RadioGroup row name="timneframe" value={timeframeByRegion} onChange={(event) => { setTimeframeByRegion(event.target.value); }}>
                                        <FormControlLabel value="1" control={<Radio />} label="This Year" />
                                        <FormControlLabel value="2" control={<Radio />} label="Last 365 Days" />
                                    </RadioGroup>
                                </FormControl>
                            </Stack>
                            <Bar
                                data={reports.milestonesReachedPerRegion}
                                indexBy="milestone"
                                keys={reports[`milestoneReachedPerRegion${numberOrPercentByRegion[1]}${parseInt(timeframeByRegion) === 1 ? 'ThisYear' : 'Last365Days'}Keys`]}
                                layout="vertical"
                                groupMode="grouped"
                                valueScale={{ type: 'linear' }}
                                indexScale={{ type: 'band', round: true }}
                                maxValue={numberOrPercentByRegion[0] === 'number' ? 2000 : 50}
                                enableLabel={true}
                                label={(data) => {
                                    if (numberOrPercentByRegion[0] === 'number') {
                                        if (data.value < 9) {
                                            return <tspan y="-10">{data.value}</tspan>;
                                        }
                                    } else {
                                        if (data.value === 0) {
                                            return <tspan y="-10">0</tspan>;
                                        }
                                    }
                                    return null;
                                }}
                                axisTop={null}
                                axisRight={null}
                                axisLeft={{
                                    format: " >-,",
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: numberOrPercentByRegion[0] === 'number' ? 'Number of Projects' : '% Change from Previous Year',
                                    legendPosition: 'middle',
                                    legendOffset: -60,
                                }}
                                colors={({ id, data }) => String(data[`color${id.substring(id.indexOf("-"))}`])}
                                width={1000}
                                height={400}
                                padding={0.3}
                                margin={{ top: 60, right: 0, bottom: 120, left: 80, }}
                                legendLabel={datum => `${filters.stateorprovinces[parseInt(datum.id.split('-')[2])].abbreviation}`}
                                legends={[
                                    {
                                        dataFrom: 'keys',
                                        anchor: 'bottom-left',
                                        direction: 'row',
                                        justify: false,
                                        itemWidth: 80,
                                        itemHeight: 20,
                                        translateX: -40,
                                        translateY: 80,
                                    },
                                ]}
                                tooltip={({ id, value, index, indexValue, data }) => {
                                    const regionIndex = parseInt(id.split('-')[2]);
                                    const milestoneIndex = index;
                                    let formattedValue = null;
                                    if (numberOrPercentByRegion[0] === 'number') {
                                        formattedValue = dataUtils.formatNumberValue(value);
                                    } else {
                                        if (value > 1000) {
                                            formattedValue = '> 1,000%';
                                        } else {
                                            formattedValue = `${dataUtils.formatNumberValue(Math.round(value * 100) / 100)}%`;
                                        }
                                    }
                                    return (
                                        <div
                                            style={{
                                                padding: 10,
                                                boxShadow: 'rgb(0 0 0 / 20%) 0px 3px 3px -2px, rgb(0 0 0 / 14%) 0px 3px 4px 0px, rgb(0 0 0 / 12%) 0px 1px 8px 0px',
                                                backgroundColor: '#FFFFFF',
                                                fontSize: 13
                                            }}
                                        >
                                            <table style={{ width: '100%', marginBottom: 20, }}>
                                                <tbody>
                                                    <tr>
                                                        <td>Region:</td>
                                                        <td style={{ fontWeight: 'bold', }}>{filters.stateorprovinces[regionIndex].label}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Timeframe:</td>
                                                        <td style={{ fontWeight: 'bold', }}>{parseInt(timeframeByRegion) === 1 ? 'This Year' : 'Last 365 Days'}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Milestone:</td>
                                                        <td style={{ fontWeight: 'bold', }}>{indexValue}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Description:</td>
                                                        <td>{MILESTONE_DESCRIPTION[milestoneIndex]}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table style={{ width: '100%', }}>
                                                <tbody>
                                                    <tr>
                                                        <td>{numberOrPercentByRegion[0] === 'number' ? 'Total Number of Projects' : '% Change from Previous Year'}:</td>
                                                        <td style={{ fontWeight: 'bold', }}>{formattedValue}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Number of Projects with Value Below 5M:</td>
                                                        <td style={{ fontWeight: 'bold', }}>{dataUtils.formatNumberValue(data[`numberOfProjectsBelow5m-${timeframeByRegion}-${regionIndex}`])}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Number of Projects with Value Between 5M and 30M:</td>
                                                        <td style={{ fontWeight: 'bold', }}>{dataUtils.formatNumberValue(data[`numberOfProjectsBetween5mAnd30m-${timeframeByRegion}-${regionIndex}`])}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Number of Projects with Value Above 30M:</td>
                                                        <td style={{ fontWeight: 'bold', }}>{dataUtils.formatNumberValue(data[`numberOfProjectsAbove30m-${timeframeByRegion}-${regionIndex}`])}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total Project Value:</td>
                                                        <td style={{ fontWeight: 'bold', }}>${dataUtils.formatNumberValue(data[`totalValue-${timeframeByRegion}-${regionIndex}`])}M</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    );
                                }}
                                animate={true}
                                isInteractive={true}
                            />
                        </Stack>
                    </Grid>
                </Grid>
                <Dialog
                    open={alertDialogOpen}
                    onClose={() => { setAlertDialogOpen(false); }}
                >
                    <DialogTitle id="alert-dialog-title">{alertDialogTitle}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">{alertDialogContent}</DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => { setAlertDialogOpen(false); }} autoFocus>OK</Button>
                    </DialogActions>
                </Dialog>
                <Backdrop
                    sx={{ color: '#FFFFFF', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                    open={backdropOpen}
                >
                    <CircularProgress color="inherit" />
                </Backdrop>
            </Box>
        );
    }
}

export default ProjectMilestones;